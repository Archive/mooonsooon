/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#ifndef __PREFS_H__
#define __PREFS_H__


void msooon_prefs_show (GtkWidget * widget, VDEMasterPart * masterpart);


#endif /* __PREFS_H__ */
