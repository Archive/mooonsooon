/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */

/* no corba for now
   #include <orb/orb.h>
 */

#include "util.h"

guint
mooonsooon_long_hash (gconstpointer key)
{
   return 0;			/*(guint) *((const CORBA_long*) key); */
}

gint
mooonsooon_long_equal (gconstpointer a, gconstpointer b)
{
   return 0;
/**((const CORBA_long*) a) == *((const CORBA_long*) b);*/
}

guint
mooonsooon_short_hash (gconstpointer key)
{
   return (guint) * ((const gshort *)key);
}

gint
mooonsooon_short_equal (gconstpointer a, gconstpointer b)
{
   return *((const gshort *)a) == *((const gshort *)b);
}
