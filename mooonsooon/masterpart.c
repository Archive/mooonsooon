/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#include <config.h>
#include <gnome.h>
#include <liboaf/liboaf.h>
#include <bonobo.h>
#include <bonobo/bonobo-property-types.h>
#include <ORBitservices/CosEventChannel.h>

#include "event-channel.h"
#include "masterpart.h"

static GtkObjectClass *vde_masterpart_parent_class;

static CORBA_Object create_vde_masterpart (BonoboObject *object);
static void masterpart_destroy (GtkObject *object);
static void masterpart_class_init (VDEMasterPartClass *class);
static void masterpart_init (VDEMasterPart *masterpart);
static VDE_Part impl_get_part (PortableServer_Servant servant,
			       CORBA_char *repo_id,
			       CORBA_Environment *ev);
static Bonobo_UIHandler impl_get_ui_handler (PortableServer_Servant servant,
					    CORBA_Environment *ev);
static void impl_register_window (PortableServer_Servant servant,
				  Bonobo_Control window,
				  CORBA_Environment *ev);
static void impl_register_proppage (PortableServer_Servant servant,
				    Bonobo_Control page,
				    CORBA_Environment *ev);
static CORBA_unsigned_short
impl_get_event_class_id (PortableServer_Servant servant,
			 CORBA_char *string,
			 CORBA_Environment *ev);
static CORBA_Object impl_connect_sender (PortableServer_Servant servant,
					 CORBA_Object supplier,
					 CORBA_Environment *ev);
static CORBA_Object impl_connect_listener (PortableServer_Servant servant,
					   CORBA_Object consumer,
					   CORBA_Environment *ev);
static void event_init (VDEMasterPart *masterpart);

static POA_VDE_MasterPart__vepv vde_masterpart_vepv;

GtkType
vde_masterpart_get_type (void)
{
   static GtkType type = 0;

   if (!type) {
      GtkTypeInfo info = {
	 "IDL:VDE/MasterPart:1.0",
	 sizeof (VDEMasterPart),
	 sizeof (VDEMasterPartClass),
	 (GtkClassInitFunc) masterpart_class_init,
	 (GtkObjectInitFunc) masterpart_init,
	 NULL, NULL,
      };

      type = gtk_type_unique (bonobo_object_get_type (), &info);
   }
   return type;
}

VDEMasterPart *
vde_masterpart_new (GtkWidget *app, GnomeUIInfo *uii)
{
   VDE_MasterPart corba_masterpart;
   VDEMasterPart *masterpart;
   BonoboUIHandlerMenuItem *menu_list;

   masterpart = gtk_type_new (vde_masterpart_get_type ());
   corba_masterpart = (VDE_MasterPart)
      create_vde_masterpart (BONOBO_OBJECT (masterpart));
   if (corba_masterpart == CORBA_OBJECT_NIL) {
      gtk_object_destroy (GTK_OBJECT (masterpart));
      return NULL;
   }
   bonobo_object_construct (BONOBO_OBJECT (masterpart), corba_masterpart);

   event_init (masterpart);
   masterpart->uih = bonobo_ui_handler_new ();

   bonobo_ui_handler_set_statusbar (masterpart->uih,
				   GNOME_APP (app)->statusbar);
   bonobo_ui_handler_set_app (masterpart->uih, GNOME_APP (app));
   bonobo_ui_handler_create_menubar (masterpart->uih);
   menu_list =
      bonobo_ui_handler_menu_parse_uiinfo_list_with_data (uii, masterpart);
   bonobo_ui_handler_menu_add_list (masterpart->uih, "/", menu_list);
   
   bonobo_ui_handler_menu_free_list (menu_list);

   
   masterpart->app = app;
   return masterpart;
}

void
vde_masterpart_init (VDEMasterPart *masterpart)
{
/*
   CORBA_Object part;
   CORBA_Environment ev;
   CORBA_char *name;
   gchar *str;
*/
   
   /* start first part we find so we can test a bit */
/*
   CORBA_exception_init (&ev);
   part = goad_server_activate_with_repo_id (NULL, "IDL:VDE/Part:1.0", 0, NULL);
   name = VDE_Part__get_name ((VDE_Part) part, &ev);
   g_hash_table_insert (masterpart->parts, name, part);
   VDE_Part_init ((VDE_Part) part,
		  bonobo_object_corba_objref (BONOBO_OBJECT (masterpart)), &ev);
   CORBA_exception_free (&ev);
   str = g_strdup_printf ("with name: %s", name);
   vde_event_supplier_push (masterpart->supplier, 1, 1, "Adding part",
			    bonobo_property_marshal_string ("string",
							   str, NULL));
   g_free (str);
*/
   OAF_ServerInfoList *sil;
   OAF_ServerInfo *si;
   CORBA_Object part;
   CORBA_Environment ev;
   CORBA_char *name;
   gchar *str;
   gchar *aid;
   gint i;

   g_warning ("0");
//   if (!oaf_is_initialized ())
//      return;

   g_warning ("1");
   CORBA_exception_init (&ev);
   /* get a list of parts */
   sil = oaf_query ("repo_ids.has('IDL:VDE/Part:1.0')", NULL, &ev);
   if (ev._major != CORBA_NO_EXCEPTION)
      g_warning (ev._repo_id);


   /* TODO: check if minimum parts are available (???) */

   g_warning ("2");
   /* start parts */
   for (i = 0; i < sil->_length; i++) {
      si = &(sil->_buffer[i]);
      /* TODO: check if part needs starting */
      aid = g_strconcat ("OAFAID:[", si->iid, "]");
      g_warning ("starting %s", aid);
      part = oaf_activate_from_id (aid, 0, NULL, &ev);
      if (!CORBA_Object_is_nil (part, &ev)
	  && (ev._major = CORBA_NO_EXCEPTION)) {
	 /* call part->init */

	 name = VDE_Part__get_name ((VDE_Part) part, &ev);
	 VDE_Part_init ((VDE_Part) part,
			bonobo_object_corba_objref (BONOBO_OBJECT (masterpart)), &ev);
	 str = g_strdup_printf ("with name: %s", name);
	 vde_event_supplier_push (masterpart->supplier, 1, 1, "Adding part",
				  bonobo_property_marshal_string ("string",
								 str, NULL));
	 g_free (str);
      }
   }
   g_warning ("3");
   CORBA_exception_free (&ev);
   CORBA_free (sil);
}

POA_VDE_MasterPart__epv *
vde_masterpart_get_epv (void)
{
   POA_VDE_MasterPart__epv *epv;

   epv = g_new0 (POA_VDE_MasterPart__epv, 1);
   epv->get_part = impl_get_part;
   epv->get_ui_handler = impl_get_ui_handler;
   epv->register_window = impl_register_window;
   epv->register_proppage = impl_register_proppage;
   epv->get_event_class_id = impl_get_event_class_id;
   epv->connect_sender = impl_connect_sender;
   epv->connect_listener = impl_connect_listener;

   return epv;
}

static void
init_masterpart_corba_class (void)
{
   vde_masterpart_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
   vde_masterpart_vepv.VDE_MasterPart_epv = vde_masterpart_get_epv ();
}

static void
register_dock_window (VDEMasterPart *mp, BonoboControlFrame *cf)
{
   BonoboPropertyBagClient *pbc;
   gchar *name;
   gboolean visible = FALSE;
   GtkWidget *dockchild, *dock;


   pbc = bonobo_control_frame_get_control_property_bag (cf);
   name = bonobo_property_bag_client_get_value_string (pbc, "title");
   g_hash_table_insert (mp->dock_windows, name, cf);

   /* TODO: check settings if this should be visible */
   if (visible) {
      /* TODO: add to dock */
   }
   /* for now always show to test */
   dockchild = gnome_dock_item_new (name, 0);
   gtk_container_add (GTK_CONTAINER (dockchild),
				     bonobo_control_frame_get_widget (cf));
   gtk_widget_show_all (dockchild);
   dock = GNOME_APP (mp->app)->dock;
   gnome_app_add_dock_item (GNOME_APP (mp->app), GNOME_DOCK_ITEM (dockchild),
			    GNOME_DOCK_BOTTOM ,0, 1, 0);

}

static void
register_listdock_window (VDEMasterPart *mp, BonoboControlFrame *cf)
{
   BonoboPropertyBagClient *pbc;
   gchar *name;
   gboolean visible = FALSE;

   pbc = bonobo_control_frame_get_control_property_bag (cf);
   name = bonobo_property_bag_client_get_value_string (pbc, "title");
   g_hash_table_insert (mp->listdock_windows, name, cf);

   /* TODO: check settings if this should be visible */
   if (visible) {
      /* TODO: add to list_dock */
   }
}

static void
register_window_window (VDEMasterPart *mp, BonoboControlFrame *cf)
{
   BonoboPropertyBagClient *pbc;
   gchar *name;
   gboolean visible = FALSE;

   pbc = bonobo_control_frame_get_control_property_bag (cf);
   name = bonobo_property_bag_client_get_value_string (pbc, "title");
   g_hash_table_insert (mp->windows, name, cf);

   /* TODO: check settings if this should be visible */
   if (visible) {
      /* TODO: show */
   }
}

static void
impl_register_window (PortableServer_Servant servant,
		      Bonobo_Control window, CORBA_Environment *ev)
{
   BonoboControlFrame *control_frame;
   BonoboPropertyBagClient *pbc;
   VDE_MasterPart_WindowType type;
   VDEMasterPart *masterpart =
      VDE_MASTERPART (bonobo_object_from_servant (servant));

   control_frame = bonobo_control_frame_new ();
   bonobo_control_frame_bind_to_control (control_frame, window);
   pbc = bonobo_control_frame_get_control_property_bag (control_frame);
   type = (VDE_MasterPart_WindowType)
      bonobo_property_bag_client_get_value_ushort (pbc, "window_type");

   switch (type) { 
   case VDE_MasterPart_LIST_DOCK:
      register_listdock_window (masterpart, control_frame);
      break;
   case VDE_MasterPart_DOCK:
      register_dock_window (masterpart, control_frame);
      break;
   case VDE_MasterPart_WINDOW:
      register_window_window (masterpart, control_frame);
      break;
   }
}

static void
impl_register_proppage (PortableServer_Servant servant, Bonobo_Control page,
			CORBA_Environment *ev)
{
   BonoboControlFrame *control_frame;
   BonoboPropertyBagClient *pbc;
   gchar *path;
   VDEMasterPart *masterpart =
      VDE_MASTERPART (bonobo_object_from_servant (servant));

   /* TODO: get properties */
   control_frame = bonobo_control_frame_new ();
   bonobo_control_frame_bind_to_control (control_frame, page);
   pbc = bonobo_control_frame_get_control_property_bag (control_frame);
   path = (gchar *)
      bonobo_property_bag_client_get_value_string (pbc, "path");
   g_warning ("inserting proppage with path %s", path);

   g_hash_table_insert (masterpart->prop_pages, path, control_frame);
}

static CORBA_unsigned_short
impl_get_event_class_id (PortableServer_Servant servant, CORBA_char *string,
			 CORBA_Environment *ev)
{
}

static CORBA_Object
impl_connect_sender (PortableServer_Servant servant,
		     CORBA_Object supplier, CORBA_Environment *ev)
{
   CosEventChannelAdmin_ProxyPushConsumer retval;
   CosEventChannelAdmin_SupplierAdmin admin;
   VDEMasterPart *masterpart =
      VDE_MASTERPART (bonobo_object_from_servant (servant));
   BonoboObject *gobject = BONOBO_OBJECT (masterpart);

   admin = CosEventChannelAdmin_EventChannel_for_suppliers
      (masterpart->event_channel, ev);
   if (ev->_major != CORBA_NO_EXCEPTION) {
      return CORBA_OBJECT_NIL;
   }
   retval =
      CosEventChannelAdmin_SupplierAdmin_obtain_push_consumer (admin, ev);
   if (ev->_major != CORBA_NO_EXCEPTION) {
      return CORBA_OBJECT_NIL;
   }
   CosEventChannelAdmin_ProxyPushConsumer_connect_push_supplier (retval,
								 supplier,
								 ev);
   if (ev->_major != CORBA_NO_EXCEPTION) {
      return CORBA_OBJECT_NIL;
   }
   return retval;
}

static CORBA_Object
impl_connect_listener (PortableServer_Servant servant,
		       CORBA_Object consumer, CORBA_Environment *ev)
{
   CosEventChannelAdmin_ProxyPushSupplier retval;
   CosEventChannelAdmin_ConsumerAdmin admin;
   VDEMasterPart *masterpart =
      VDE_MASTERPART (bonobo_object_from_servant (servant));
   BonoboObject *gobject = BONOBO_OBJECT (masterpart);

   admin =
      CosEventChannelAdmin_EventChannel_for_consumers(masterpart->event_channel,
						      ev);
   if (ev->_major != CORBA_NO_EXCEPTION) {
      return CORBA_OBJECT_NIL;
   }
   retval =
      CosEventChannelAdmin_ConsumerAdmin_obtain_push_supplier (admin, ev);
   if (ev->_major != CORBA_NO_EXCEPTION) {
      return CORBA_OBJECT_NIL;
   }
   CosEventChannelAdmin_ProxyPushSupplier_connect_push_consumer (retval,
								 consumer,
								 ev);
   if (ev->_major != CORBA_NO_EXCEPTION) {
      return CORBA_OBJECT_NIL;
   }

   return retval;
}

static VDE_Part
impl_get_part (PortableServer_Servant servant,
	       CORBA_char *repo_id, CORBA_Environment *ev)
{
}

static Bonobo_UIHandler
impl_get_ui_handler (PortableServer_Servant servant, CORBA_Environment *ev)
{
   VDEMasterPart *masterpart =
      VDE_MASTERPART (bonobo_object_from_servant (servant));
   
   return (Bonobo_UIHandler) CORBA_Object_duplicate
      (bonobo_object_corba_objref (BONOBO_OBJECT (masterpart->uih)), ev);
}

static CORBA_Object
create_vde_masterpart (BonoboObject *object)
{
   POA_VDE_MasterPart *servant;
   CORBA_Environment ev;

   servant = (POA_VDE_MasterPart *) g_new0 (VDEMasterPart, 1);
   servant->vepv = &vde_masterpart_vepv;

   CORBA_exception_init (&ev);
   POA_VDE_MasterPart__init ((PortableServer_Servant) servant, &ev);

   if (ev._major != CORBA_NO_EXCEPTION) {
      g_free (servant);
      CORBA_exception_free (&ev);
      return CORBA_OBJECT_NIL;
   }
   CORBA_exception_free (&ev);
   return bonobo_object_activate_servant (object, servant);
}

static void
masterpart_destroy (GtkObject *object)
{
   GList *l;
   VDEMasterPart *masterpart = VDE_MASTERPART (object);

   /* TODO: free all GList 's */
   GTK_OBJECT_CLASS (vde_masterpart_parent_class)->destroy (object);
}

static void
masterpart_class_init (VDEMasterPartClass *klass)
{
   GtkObjectClass *object_class = (GtkObjectClass *) klass;

   vde_masterpart_parent_class = gtk_type_class (bonobo_object_get_type ());
   object_class->destroy = masterpart_destroy;
   init_masterpart_corba_class ();
}

static void
masterpart_init (VDEMasterPart *masterpart)
{
   masterpart->container = bonobo_container_new ();
   masterpart->app = NULL;

   masterpart->parts = g_hash_table_new (g_str_hash, g_str_equal);
   masterpart->dock_windows = g_hash_table_new (g_str_hash, g_str_equal);
   masterpart->listdock_windows = g_hash_table_new (g_str_hash, g_str_equal);
   masterpart->windows = g_hash_table_new (g_str_hash, g_str_equal);
   masterpart->prop_pages = g_hash_table_new (g_str_hash, g_str_equal);
}

static void
event_init (VDEMasterPart *masterpart)
{
   CORBA_Environment ev;
   CosEventChannelAdmin_EventChannel channel;

   CORBA_exception_init (&ev);

   /* start event channel */
   channel =
      impl_CosEventChannelAdmin_EventChannel__create (bonobo_poa (), &ev);
   masterpart->event_channel = channel;

   /* create supplier so we can send events */
   masterpart->supplier = vde_event_supplier_new ();
   vde_event_supplier_connect (masterpart->supplier,
			       BONOBO_OBJECT (masterpart));

   CORBA_exception_free (&ev);
}
