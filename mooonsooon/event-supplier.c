/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#include <gtk/gtksignal.h>
#include <gtk/gtkmarshal.h>
#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-object.h>
#include "event-supplier.h"
#include "vde.h"

enum {
   ON_DISCONNECT,
   LAST_SIGNAL
};

static guint supplier_signals[LAST_SIGNAL];
static GtkObjectClass *vde_event_supplier_parent_class;

static CORBA_Object create_vde_event_supplier (BonoboObject *object);
static void event_supplier_destroy (GtkObject *object);
static void event_supplier_class_init (VDEEventSupplierClass *class);
static void event_supplier_init (VDEEventSupplier *event_supplier);

static void impl_disconnect_push_supplier (PortableServer_Servant servant,
					   CORBA_Environment *ev);

static POA_VDE_EventSupplier__vepv vde_event_supplier_vepv;

GtkType
vde_event_supplier_get_type (void)
{
   static GtkType type = 0;

   if (!type) {
      GtkTypeInfo info = {
	 "IDL:VDE/EventSupplier:1.0",
	 sizeof (VDEEventSupplier),
	 sizeof (VDEEventSupplierClass),
	 (GtkClassInitFunc) event_supplier_class_init,
	 (GtkObjectInitFunc) event_supplier_init,
	 NULL, NULL,
      };

      type = gtk_type_unique (bonobo_object_get_type (), &info);
   }
   return type;
}

VDEEventSupplier *
vde_event_supplier_new (void)
{
   VDE_EventSupplier corba_event_supplier;
   VDEEventSupplier *event_supplier;

   event_supplier = gtk_type_new (vde_event_supplier_get_type ());
   corba_event_supplier = (VDE_EventSupplier)
      create_vde_event_supplier (BONOBO_OBJECT (event_supplier));
   if (corba_event_supplier == CORBA_OBJECT_NIL) {
      gtk_object_destroy (GTK_OBJECT (event_supplier));
      return NULL;
   }
   bonobo_object_construct (BONOBO_OBJECT (event_supplier),
			   corba_event_supplier);

   return event_supplier;
}

void
vde_event_supplier_connect (VDEEventSupplier *supplier,
			    BonoboObject *masterpart)
{
   CORBA_Environment ev;

   CORBA_exception_init (&ev);
   supplier->proxypushconsumer = (CORBA_Object)
      VDE_MasterPart_connect_sender (bonobo_object_corba_objref (masterpart),
				     bonobo_object_corba_objref (BONOBO_OBJECT
								(supplier)),
				     &ev);
   CORBA_exception_free (&ev);
}

void
vde_event_supplier_push (VDEEventSupplier *supplier, guint class, guint id,
			 gchar *description, CORBA_any *data)
{
   CORBA_any *any;
   CORBA_Environment ev;
   VDE_Event *event;
   CORBA_Object proxy = supplier->proxypushconsumer;

   event = VDE_Event__alloc ();
   event->class_id = class;
   event->event_id = id;
   event->description = CORBA_string_dup (description);
   event->data = *data;

   any = CORBA_any__alloc ();
   any->_type = (CORBA_TypeCode) TC_VDE_Event;
   any->_value = event;
   CORBA_any_set_release (any, TRUE);

   CORBA_exception_init (&ev);
   g_warning ("sending the event");
   CosEventComm_PushConsumer_push (proxy, any, &ev);
   CORBA_exception_free (&ev);
}

POA_CosEventComm_PushSupplier__epv *
vde_event_supplier_get_coseventepv (void)
{
   POA_CosEventComm_PushSupplier__epv *epv;

   epv = g_new0 (POA_CosEventComm_PushSupplier__epv, 1);
   epv->disconnect_push_supplier = impl_disconnect_push_supplier;

   return epv;
}

POA_VDE_EventSupplier__epv *
vde_event_supplier_get_epv (void)
{
   POA_VDE_EventSupplier__epv *epv;

   epv = g_new0 (POA_VDE_EventSupplier__epv, 1);

   return epv;
}

static void
init_supplier_corba_class (void)
{
   vde_event_supplier_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
   vde_event_supplier_vepv.CosEventComm_PushSupplier_epv =
      vde_event_supplier_get_coseventepv ();
   vde_event_supplier_vepv.VDE_EventSupplier_epv =
      vde_event_supplier_get_epv ();
}

static void
impl_disconnect_push_supplier (PortableServer_Servant servant,
			       CORBA_Environment *ev)
{
}

static CORBA_Object
create_vde_event_supplier (BonoboObject *object)
{
   POA_VDE_EventSupplier *servant;
   CORBA_Object ret;
   CORBA_Environment ev;

   CORBA_exception_init (&ev);

   servant = (POA_VDE_EventSupplier *) g_new0 (VDEEventSupplier, 1);
   servant->vepv = &vde_event_supplier_vepv;

   POA_VDE_EventSupplier__init ((PortableServer_Servant) servant, &ev);

   if (ev._major != CORBA_NO_EXCEPTION) {
      g_free (servant);
      CORBA_exception_free (&ev);
      return CORBA_OBJECT_NIL;
   }
   CORBA_exception_free (&ev);
   return bonobo_object_activate_servant (object, servant);
}

static void
event_supplier_destroy (GtkObject *object)
{
   VDEEventSupplier *event_supplier = VDE_EVENT_SUPPLIER (object);

   if (event_supplier->proxypushconsumer) {
      gtk_object_destroy (GTK_OBJECT (event_supplier->proxypushconsumer));
   }
   GTK_OBJECT_CLASS (vde_event_supplier_parent_class)->destroy (object);
}

static void
event_supplier_class_init (VDEEventSupplierClass *klass)
{
   GtkObjectClass *object_class = (GtkObjectClass *) klass;

   vde_event_supplier_parent_class =
      gtk_type_class (bonobo_object_get_type ());

   supplier_signals[ON_DISCONNECT] =
      gtk_signal_new ("on_disconnect",
		      GTK_RUN_FIRST,
		      object_class->type,
		      GTK_SIGNAL_OFFSET (VDEEventSupplierClass,
					 on_disconnect),
		      gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);
   gtk_object_class_add_signals (object_class, supplier_signals, LAST_SIGNAL);

   klass->on_disconnect = NULL;
   object_class->destroy = event_supplier_destroy;

   init_supplier_corba_class ();
}

static void
event_supplier_init (VDEEventSupplier *event_supplier)
{
   event_supplier->proxypushconsumer = NULL;
}
