/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#ifndef __MASTERPART_H__
#define __MASTERPART_H__

#include <gnome.h>
#include <bonobo.h>

#include "vde-masterpart.h"
#include "event-supplier.h"

BEGIN_GNOME_DECLS


#define VDE_MASTERPART_TYPE        (vde_masterpart_get_type ())
#define VDE_MASTERPART(o)          (GTK_CHECK_CAST ((o), VDE_MASTERPART_TYPE, VDEMasterPart))
#define VDE_MASTERPART_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), VDE_MASTERPART_TYPE, VDEMasterPartClass))
#define VDE_IS_MASTERPART(o)       (GTK_CHECK_TYPE ((o), VDE_MASTERPART_TYPE))
#define VDE_IS_MASTERPART_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), VDE_MASTERPART_TYPE))

typedef struct {
   BonoboObject parent;

   BonoboContainer *container;
   GtkWidget *app;
   BonoboUIHandler *uih;

   GHashTable *parts;
   GHashTable *dock_windows;
   GHashTable *listdock_windows;
   GtkWidget *listdock;
   GHashTable *windows;
   GHashTable *prop_pages;

   CORBA_Object event_channel;
   VDEEventSupplier *supplier;

} VDEMasterPart;

typedef struct {
   BonoboObjectClass parent_class;

} VDEMasterPartClass;

GtkType vde_masterpart_get_type (void);
VDEMasterPart *vde_masterpart_new (GtkWidget *app, GnomeUIInfo *uii);
void vde_masterpart_init (VDEMasterPart *masterpart);

POA_VDE_MasterPart__epv *vde_masterpart_get_epv (void);


END_GNOME_DECLS

#endif /* __MASTERPART_H__ */
