/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#ifndef __MASTERPART_CLIENT_H__
#define __MASTERPART_CLIENT_H__

#include "part.h"

BEGIN_GNOME_DECLS


void vde_masterpart_client_notify_status (BonoboObjectClient *master,
					  gchar *status);
VDEPart *vde_masterpart_client_get_part (BonoboObjectClient *master,
					 gchar *repo_id);
Bonobo_UIHandler vde_masterpart_client_get_ui_handler (BonoboObjectClient *master);


END_GNOME_DECLS

#endif /* __MASTERPART_CLIENT_H__ */
