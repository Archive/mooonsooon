/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-control-frame.h>
#include <glade/glade.h>

#include "masterpart.h"
#include "event-supplier.h"
#include "msooon-propbox.h"
#include "prefs.h"

static void add_page (gchar *title,
		      BonoboControlFrame *cf,
		      MSooonPropBox *propbox);
static void on_cancel (GtkWidget *widget,
		       VDEMasterPart *mp);
static void on_apply (GtkWidget *widget,
		      gint page,
		      VDEMasterPart *mp);

static void add_local_pages (GtkWidget *propbox);

static GtkWidget *propbox = NULL;
void msooon_propbox_create (GtkWidget *widget, VDEMasterPart *masterpart);
void
msooon_prefs_show (GtkWidget *widget, VDEMasterPart *masterpart)
{
   if (!propbox)
      msooon_propbox_create(widget, masterpart);
   gtk_widget_show (propbox);
}

void
msooon_propbox_create (GtkWidget *widget, VDEMasterPart *masterpart)
{

   propbox = msooon_prop_box_new ();
   gnome_dialog_close_hides (GNOME_DIALOG (propbox), TRUE);
   gtk_signal_connect (GTK_OBJECT (propbox), "destroy",
		       GTK_SIGNAL_FUNC (on_cancel), masterpart);
   gtk_signal_connect (GTK_OBJECT (propbox), "apply",
		       GTK_SIGNAL_FUNC (on_apply), masterpart);
   gtk_signal_connect (GTK_OBJECT (propbox), "hide",
		       GTK_SIGNAL_FUNC (on_cancel), masterpart);
   add_local_pages (propbox);
   g_warning ("adding external pages");
   g_hash_table_foreach (masterpart->prop_pages, (GHFunc) add_page, propbox);
   g_warning ("done");

   gtk_widget_show (propbox);

}

static void
add_page (gchar *title, BonoboControlFrame *cf, MSooonPropBox *propbox)
{
   gchar *child_title;
   gchar *parent_title;

   g_warning (title);

//   msooon_prop_box_append_page (propbox, bonobo_control_frame_get_widget (frame),
//				child_title, parent_title);
   msooon_prop_box_append_page (propbox, bonobo_control_frame_get_widget (cf),
				title, NULL);
   gtk_widget_show (bonobo_control_frame_get_widget (cf));
}

static void
send_event (VDEMasterPart *mp, gint event_id)
{
   CORBA_any *any;
   CORBA_boolean *b;
   
   b = (CORBA_boolean *) CORBA_octet_allocbuf (sizeof (CORBA_boolean));
   *b = TRUE;
   
   any = CORBA_any__alloc ();
   any->_type = (CORBA_TypeCode) TC_boolean;
   any->_value = b;
   CORBA_any_set_release (any, TRUE);

   if (event_id == 1) {
      vde_event_supplier_push (VDE_EVENT_SUPPLIER (mp->supplier), 2,
			       event_id, "save settings", any);
   } else {
      vde_event_supplier_push (VDE_EVENT_SUPPLIER (mp->supplier), 2,
			       event_id, "restore settings", any);
   }
}


static void
on_cancel (GtkWidget *widget, VDEMasterPart *mp)
{
   /* TODO: use event channel to tell pages to reset settings */
   send_event (mp, 0);
//   gtk_widget_destroy (widget);
}

static void
on_apply (GtkWidget *widget, gint page, VDEMasterPart *mp)
{
   /* TODO: use event channel to tell pages to save settings */
   send_event (mp, 1);
}

static void
add_local_pages (GtkWidget *propbox)
{
   GladeXML *page;
   GtkWidget *tab;
   
   page = glade_xml_new ("props.glade", "page1");
   glade_xml_signal_autoconnect (page);
   tab = glade_xml_get_widget (page, "page1");
   msooon_prop_box_append_page (MSOOON_PROP_BOX (propbox), tab,
				"MooonSooon", NULL);
}
