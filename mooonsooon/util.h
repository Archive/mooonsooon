/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#ifndef __UTIL_H__
#define __UTIL_H__

#include <glib.h>

#define CORBAIFY_STRING(s) ((s) = NULL ? "" : (s))
#define UNCORBAIFY_STRING(s) ((strlen (s) == 0) ? NULL : (s))

guint mooonsooon_long_hash (gconstpointer key);
gint mooonsooon_long_equal (gconstpointer a, gconstpointer b);

guint mooonsooon_short_hash (gconstpointer key);
gint mooonsooon_short_equal (gconstpointer a, gconstpointer b);

#endif /* __UTIL_H__ */
