/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#ifndef __EVENT_CONSUMER_H__
#define __EVENT_CONSUMER_H__

#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-object-client.h>
#include <mooonsooon/masterpart.h>
#include <mooonsooon/vde-event-consumer.h>

BEGIN_GNOME_DECLS


#define VDE_EVENT_CONSUMER_TYPE        (vde_event_consumer_get_type ())
#define VDE_EVENT_CONSUMER(o)          (GTK_CHECK_CAST ((o), VDE_EVENT_CONSUMER_TYPE, VDEEventConsumer))
#define VDE_EVENT_CONSUMER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), VDE_EVENT_CONSUMER_TYPE, VDEEventConsumerClass))
#define VDE_IS_EVENT_CONSUMER(o)       (GTK_CHECK_TYPE ((o), VDE_EVENT_CONSUMER_TYPE))
#define VDE_IS_EVENT_CONSUMER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), VDE_EVENT_CONSUMER_TYPE))


typedef struct {
   BonoboObject parent;

   GSList *event_queue;
   GSList *queue_last;
   CORBA_Object proxypushsupplier;
} VDEEventConsumer;

typedef struct {
   BonoboObjectClass parent_class;

   void (*on_push) (VDEEventConsumer *event_consumer, gshort class_id,
		    gshort event_id, gchar *description, CORBA_any msg);
   void (*on_disconnect) (VDEEventConsumer *event_consumer);

} VDEEventConsumerClass;

GtkType vde_event_consumer_get_type (void);
VDEEventConsumer *vde_event_consumer_new (void);
void vde_event_consumer_connect (VDEEventConsumer *consumer,
				 BonoboObjectClient *masterpart);

POA_CosEventComm_PushConsumer__epv *vde_event_consumer_get_coseventepv (void);
POA_VDE_EventConsumer__epv *vde_event_consumer_get_epv (void);


END_GNOME_DECLS

#endif /* __EVENT_CONSUMER_H__ */
