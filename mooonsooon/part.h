/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#ifndef __PART_H__
#define __PART_H__

#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-object-client.h>
#include <bonobo/bonobo-embeddable.h>

#include <mooonsooon/vde-masterpart.h>
#include <mooonsooon/vde-part.h>

BEGIN_GNOME_DECLS


#define VDE_PART_TYPE        (vde_part_get_type ())
#define VDE_PART(o)          (GTK_CHECK_CAST ((o), VDE_PART_TYPE, VDEPart))
#define VDE_PART_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), VDE_PART_TYPE, VDEPartClass))
#define VDE_IS_PART(o)       (GTK_CHECK_TYPE ((o), VDE_PART_TYPE))
#define VDE_IS_PART_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), VDE_PART_TYPE))
#define VDE_PART_FACTORY(fn) ((VDEPartFactory)(fn))

typedef GtkWidget *(*VDEPartFactory) (void);

#define VDEUIINFO_END	{NULL, NULL, VDE_MENU_BEFORE, FALSE, GNOMEUIINFO_END}

typedef enum {
   VDE_MENU_BEFORE,
   VDE_MENU_AFTER
} VDEMenuPosition;

typedef struct {
   gchar *parent_path;
   gchar *relative_to_sibling;
   VDEMenuPosition pos;
   gboolean override;
   GnomeUIInfo data;
} VDEUIInfo;

typedef struct {
   BonoboObject parent;

   gchar *name;

   GList *windows;
   GList *prop_pages;
   VDEEventConsumer *consumer;
   GHashTable *event_classes;

   BonoboObjectClient *master;
   
   VDEUIInfo *uih_data;
   BonoboUIHandler *handler;
} VDEPart;

typedef struct {
   BonoboObjectClass parent_class;

   void (*on_init) (VDEPart *part);

} VDEPartClass;

GtkType vde_part_get_type (void);

VDEPart *vde_part_new (const gchar *name,
		       VDEUIInfo *data);
void vde_part_add_window (VDEPart *part,
			  BonoboControl *control,
			  VDE_MasterPart_WindowType type,
			  gchar *title);
void vde_part_add_proppage (VDEPart *part,
			    BonoboControl *control,
			    gchar * path);

POA_VDE_Part__epv *vde_part_get_epv (void);


END_GNOME_DECLS

#endif /* __PART_H__ */
