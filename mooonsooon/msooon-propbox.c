/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#include "msooon-propbox.h"

static GnomePropertyBoxClass *parent_class = NULL;


static void msooon_prop_box_class_init (MSooonPropBoxClass *klass);
static void msooon_prop_box_init (MSooonPropBox *prop_box);
static gboolean msooon_prop_boxP_close_pages (GnomeDialog *d);
static guint msooon_box_get_pos (GtkBox *box,
				 GtkWidget *widget);
static gint my_str_equal (gconstpointer a,
			  gconstpointer b);
static void prop_boxP_row_select_cb (GtkWidget *widget,
				     GtkCTreeNode *node,
				     gint column);

guint
msooon_prop_box_get_type (void)
{
   static guint prop_box_type = 0;

   if (!prop_box_type) {
      GtkTypeInfo prop_box_info = {
	 "MSooonPropBox",
	 sizeof (MSooonPropBox),
	 sizeof (MSooonPropBoxClass),
	 (GtkClassInitFunc) msooon_prop_box_class_init,
	 (GtkObjectInitFunc) msooon_prop_box_init,
	 (GtkArgSetFunc) NULL,
	 (GtkArgGetFunc) NULL
      };

      prop_box_type = gtk_type_unique (gnome_property_box_get_type (),
				       &prop_box_info);
   }
   return prop_box_type;
}

static void
msooon_prop_box_class_init (MSooonPropBoxClass *klass)
{
   GnomeDialogClass *dialog_class;

   dialog_class = (GnomeDialogClass *) klass;
   parent_class = gtk_type_class (gnome_dialog_get_type ());
   dialog_class->close = msooon_prop_boxP_close_pages;
}

static void
msooon_prop_box_init (MSooonPropBox *prop_box)
{
   guint pos;

   prop_box->hpaned = gtk_hpaned_new ();
   gtk_paned_set_gutter_size (GTK_PANED (prop_box->hpaned), 12);

   pos = msooon_box_get_pos (GTK_BOX (GNOME_DIALOG (prop_box)->vbox),
			     GNOME_PROPERTY_BOX (prop_box)->notebook);

   gtk_widget_ref (GNOME_PROPERTY_BOX (prop_box)->notebook);
   gtk_container_remove (GTK_CONTAINER (GNOME_DIALOG (prop_box)->vbox),
			 GNOME_PROPERTY_BOX (prop_box)->notebook);
   gtk_paned_add2 (GTK_PANED (prop_box->hpaned),
		   GNOME_PROPERTY_BOX (prop_box)->notebook);
   gtk_container_add (GTK_CONTAINER (GNOME_DIALOG (prop_box)->vbox),
		      prop_box->hpaned);
   gtk_box_reorder_child (GTK_BOX (GNOME_DIALOG (prop_box)->vbox),
			  prop_box->hpaned, pos);
   gtk_notebook_set_show_tabs (GTK_NOTEBOOK (
	 GNOME_PROPERTY_BOX (prop_box)->notebook), FALSE);
   gtk_notebook_set_scrollable (GTK_NOTEBOOK (
	 GNOME_PROPERTY_BOX (prop_box)->notebook),
				TRUE);
   gtk_widget_unref (GNOME_PROPERTY_BOX (prop_box)->notebook);

   prop_box->ctree = gtk_ctree_new (1, 0);
   gtk_object_set_data (GTK_OBJECT (prop_box->ctree), "MSOOON_PROP_BOX",
			prop_box);
   /* copied from control center */
   gtk_clist_set_row_height (GTK_CLIST (prop_box->ctree), 20);
   gtk_ctree_set_line_style (GTK_CTREE (prop_box->ctree),
			     GTK_CTREE_LINES_DOTTED);
   gtk_ctree_set_expander_style (GTK_CTREE (prop_box->ctree),
				 GTK_CTREE_EXPANDER_SQUARE);
   gtk_clist_set_column_width (GTK_CLIST (prop_box->ctree), 0, 125);
   gtk_clist_set_column_auto_resize (GTK_CLIST (prop_box->ctree), 0, TRUE);
   gtk_clist_set_selection_mode (GTK_CLIST (prop_box->ctree),
				 GTK_SELECTION_BROWSE);
   gtk_signal_connect (GTK_OBJECT (prop_box->ctree), "tree_select_row",
		       GTK_SIGNAL_FUNC (prop_boxP_row_select_cb), NULL);

   prop_box->scrolled_win =
      gtk_scrolled_window_new (GTK_CLIST (prop_box->ctree)->hadjustment,
			       GTK_CLIST (prop_box->ctree)->vadjustment);
   gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
				   (prop_box->scrolled_win),
				   GTK_POLICY_AUTOMATIC,
				   GTK_POLICY_AUTOMATIC);
   gtk_widget_set_usize (prop_box->scrolled_win, 150, 200);
   gtk_container_add (GTK_CONTAINER (prop_box->scrolled_win),
		      prop_box->ctree);
   gtk_widget_show (prop_box->ctree);
   gtk_paned_add1 (GTK_PANED (prop_box->hpaned), prop_box->scrolled_win);
   gtk_widget_show (prop_box->scrolled_win);

   gtk_widget_show (prop_box->hpaned);
}

GtkWidget *
msooon_prop_box_new (void)
{
   return gtk_type_new (msooon_prop_box_get_type ());
}

guint32
msooon_prop_box_append_page (MSooonPropBox *box, GtkWidget *child,
			     gchar *title, gchar *parent_title)
{
   GtkCTreeNode *new_node, *parent_node = NULL;
   gchar *text[1], *str;

   g_return_val_if_fail (box != NULL, 0);
   g_return_val_if_fail (MSOOON_IS_PROP_BOX (box), 0);
   g_return_val_if_fail (child != NULL, 0);
   g_return_val_if_fail (GTK_IS_WIDGET (child), 0);
   g_return_val_if_fail (title != NULL, 0);

   text[0] = str = g_strdup (title);
   gtk_object_set_data (GTK_OBJECT (child), "MSOOON_PREFTREE_TITLE", str);

   if (parent_title)
      parent_node = gtk_ctree_find_by_row_data_custom (GTK_CTREE (box->ctree),
						       NULL, parent_title,
						       my_str_equal);

   gtk_notebook_append_page (GTK_NOTEBOOK
			     (GNOME_PROPERTY_BOX (box)->notebook), child,
			     NULL);
   new_node =
      gtk_ctree_insert_node (GTK_CTREE (box->ctree), parent_node, NULL, text,
			     5, NULL, NULL, NULL, NULL, FALSE, TRUE);
   gtk_ctree_node_set_row_data (GTK_CTREE (box->ctree), new_node, child);
}

static gint
my_str_equal (gconstpointer a, gconstpointer b)
{
   gchar *str = NULL;

   g_return_val_if_fail (GTK_IS_OBJECT (a), -1);

   str = gtk_object_get_data (GTK_OBJECT (a), "MSOOON_PREFTREE_TITLE");
   return strcmp (str, (const gchar *)b);
}

static guint
msooon_box_get_pos (GtkBox *box, GtkWidget *widget)
{
   GList *children;
   GtkBoxChild *child;
   guint pos = 0;

   children = box->children;
   while (children) {
      child = children->data;
      if (widget == child->widget)
	 return pos;
      pos++;
      children = children->next;
   }
   g_warning (_("Widget not found in box"));
   return -1;
}

static gboolean
msooon_prop_boxP_close_pages (GnomeDialog *d)
{
   /* TODO: tell each remote page to close */
   return FALSE;
}

static void
prop_boxP_row_select_cb (GtkWidget *widget, GtkCTreeNode *node, gint column)
{
   GtkWidget *pbox, *notebook, *child;

   pbox = gtk_object_get_data (GTK_OBJECT (widget), "MSOOON_PROP_BOX");
   notebook = GNOME_PROPERTY_BOX (pbox)->notebook;
   child = gtk_ctree_node_get_row_data (GTK_CTREE (widget), node);
   gtk_notebook_set_page (GTK_NOTEBOOK (notebook),
			  gtk_notebook_page_num (GTK_NOTEBOOK (notebook),
						 child));
}
