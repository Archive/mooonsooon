/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#include <config.h>
#include <gnome.h>
#include <glade/glade.h>

#include <libgnorba/gnorba.h>
#include <bonobo.h>
#include "masterpart.h"
#include "prefs.h"

CORBA_Environment ev;
CORBA_ORB orb;

static void
destroy_window (GtkWidget *widget, gpointer data)
{
   gtk_exit (0);
}

static GnomeUIInfo main_menu_file[] = {

   GNOMEUIINFO_SEPARATOR,
   GNOMEUIINFO_MENU_EXIT_ITEM (destroy_window, NULL),
   GNOMEUIINFO_END
};

static GnomeUIInfo main_menu_settings[] = {

   GNOMEUIINFO_MENU_PREFERENCES_ITEM (msooon_prefs_show, NULL),
   GNOMEUIINFO_END
};

static GnomeUIInfo main_menu_window[] = {
   GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[] = {

   GNOMEUIINFO_MENU_FILE_TREE (main_menu_file),
   GNOMEUIINFO_MENU_SETTINGS_TREE (main_menu_settings),
   GNOMEUIINFO_MENU_WINDOWS_TREE (main_menu_window),
   GNOMEUIINFO_END
};

static gint
event_source (VDEMasterPart *master)
{
   CORBA_any *any;
   CORBA_boolean *b;
   
   b = (CORBA_boolean *) CORBA_octet_allocbuf (sizeof (CORBA_boolean));
   *b = TRUE;
   
   any = CORBA_any__alloc ();
   any->_type = (CORBA_TypeCode) TC_boolean;
   any->_value = b;
   CORBA_any_set_release (any, TRUE);

   vde_event_supplier_push (VDE_EVENT_SUPPLIER (master->supplier), 1, 1, "bleh", any);

   return TRUE;
}

static guint
create_everything (void)
{
   GtkWidget *app;
   GtkWidget *appbar;
   VDEMasterPart *mp;

   app = gnome_app_new (PACKAGE, VERSION);

   gtk_signal_connect (GTK_OBJECT (app), "destroy",
		       GTK_SIGNAL_FUNC (destroy_window), NULL);
   gtk_signal_connect (GTK_OBJECT (app), "delete_event",
		       GTK_SIGNAL_FUNC (destroy_window), NULL);

   appbar = gnome_appbar_new (TRUE, TRUE, GNOME_PREFERENCES_USER);
   gnome_app_set_statusbar (GNOME_APP (app), appbar);

   mp = vde_masterpart_new (app, main_menu);

   gtk_timeout_add (5000, (GtkFunction) event_source, mp);
   vde_masterpart_init (mp);

   gtk_widget_show_all (app);

   return FALSE;
}

int
main (int argc, char **argv)
{
   CORBA_exception_init (&ev);
   gnome_init_with_popt_table (PACKAGE, VERSION, argc, argv,
				     NULL, 0, NULL);
   orb = oaf_init (argc, argv);

   if (bonobo_init (orb, CORBA_OBJECT_NIL, CORBA_OBJECT_NIL) == FALSE) {
      g_error ("can not bonobo_init\n");
   }
   gtk_idle_add ((GtkFunction) create_everything, NULL);

   glade_gnome_init ();
   bonobo_main ();
   CORBA_exception_free (&ev);

   return 0;
}
