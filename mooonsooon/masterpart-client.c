/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-object-client.h>

#include "masterpart.h"
#include "event-consumer.h"
#include "event-supplier.h"
#include "masterpart-client.h"


Bonobo_UIHandler
vde_masterpart_client_get_ui_handler (BonoboObjectClient *master)
{
   Bonobo_UIHandler uih;
   CORBA_Environment ev;

   CORBA_exception_init (&ev);
   uih = VDE_MasterPart_get_ui_handler (bonobo_object_corba_objref (
      BONOBO_OBJECT (master)), &ev);
   CORBA_exception_free (&ev);
   
   return uih;
}
