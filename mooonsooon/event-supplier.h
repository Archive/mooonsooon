/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#ifndef __EVENT_SUPPLIER_H__
#define __EVENT_SUPPLIER_H__

#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-object-client.h>
#include "vde-event-supplier.h"

BEGIN_GNOME_DECLS


#define VDE_EVENT_SUPPLIER_TYPE        (vde_event_supplier_get_type ())
#define VDE_EVENT_SUPPLIER(o)          (GTK_CHECK_CAST ((o), VDE_EVENT_SUPPLIER_TYPE, VDEEventSupplier))
#define VDE_EVENT_SUPPLIER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), VDE_EVENT_SUPPLIER_TYPE, VDEEventSupplierClass))
#define VDE_IS_EVENT_SUPPLIER(o)       (GTK_CHECK_TYPE ((o), VDE_EVENT_SUPPLIER_TYPE))
#define VDE_IS_EVENT_SUPPLIER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), VDE_EVENT_SUPPLIER_TYPE))

typedef struct {
   BonoboObject parent;

   CORBA_Object proxypushconsumer;
} VDEEventSupplier;

typedef struct {
   BonoboObjectClass parent_class;

   void (*on_disconnect) (VDEEventSupplier *event_supplier);

} VDEEventSupplierClass;

GtkType vde_event_supplier_get_type (void);
VDEEventSupplier *vde_event_supplier_new (void);
void vde_event_supplier_connect (VDEEventSupplier *supplier,
				 BonoboObject *masterpart);
void vde_event_supplier_push (VDEEventSupplier *supplier,
			      guint class, guint id, gchar *description,
			      CORBA_any *data);

POA_CosEventComm_PushSupplier__epv *vde_event_supplier_get_coseventepv (void);
POA_VDE_EventSupplier__epv *vde_event_supplier_get_epv (void);


END_GNOME_DECLS

#endif /* __EVENT_SUPPLIER_H__ */
