/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#ifndef __MSOOON_PROP_BOX_H__
#define __MSOOON_PROP_BOX_H__


#include <gnome.h>

#define MSOOON_PROP_BOX(obj) (GTK_CHECK_CAST (obj, msooon_prop_box_get_type (), MSooonPropBox))
#define MSOOON_PROP_BOX_CLASS(klass) (GTK_CHECK_CLASS_CAST (klass, msooon_prop_box_get_type (), MSooonPropBoxClass))
#define MSOOON_IS_PROP_BOX(obj) (GTK_CHECK_TYPE (obj, msooon_prop_box_get_type ()))

typedef struct _MSooonPropBox MSooonPropBox;
typedef struct _MSooonPropBoxClass MSooonPropBoxClass;

struct _MSooonPropBox {
   GnomePropertyBox parent;

   GtkWidget *hpaned;
   GtkWidget *scrolled_win;
   GtkWidget *ctree;
};

struct _MSooonPropBoxClass {
   GnomePropertyBoxClass parent_class;
};

guint msooon_prop_box_get_type (void);
GtkWidget *msooon_prop_box_new (void);
guint32 msooon_prop_box_append_page (MSooonPropBox *box, GtkWidget *child,
				     gchar *title, gchar *parent_title);

void msooon_prop_box_load_pages (void);


#endif /* __MSOOON_PROP_BOX_H__ */
