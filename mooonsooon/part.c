/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#include <gtk/gtksignal.h>
#include <gtk/gtkmarshal.h>
#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-embeddable.h>

#include "event-consumer.h"
#include "part.h"
#include "util.h"

enum {
   ON_INIT,
   LAST_SIGNAL
};

static guint vde_part_signals[LAST_SIGNAL];
static GtkObjectClass *vde_part_parent_class;

static CORBA_Object create_vde_part (BonoboObject *object);
static void part_destroy (GtkObject *object);
static void part_class_init (VDEPartClass *class);
static void part_init (VDEPart *part);
static CORBA_char *impl__get_name (PortableServer_Servant ervant,
				   CORBA_Environment *ev);
static void register_window (BonoboControl *control,
			     VDEPart *part);
static void register_proppage (BonoboControl *control,
			       VDEPart *part);
static void impl_init (PortableServer_Servant servant,
		       CORBA_Object corba_master,
		       CORBA_Environment *ev);

static POA_VDE_Part__vepv vde_part_vepv;

GtkType
vde_part_get_type (void)
{
   static GtkType type = 0;

   if (!type) {
      GtkTypeInfo info = {
	 "IDL:VDE/Part:1.0",
	 sizeof (VDEPart),
	 sizeof (VDEPartClass),
	 (GtkClassInitFunc) part_class_init,
	 (GtkObjectInitFunc) part_init,
	 NULL, NULL,
      };

      type = gtk_type_unique (bonobo_object_get_type (), &info);
   }
   return type;
}

VDEPart *
vde_part_new (const gchar *name, VDEUIInfo *data)
{
   VDE_Part corba_part;
   VDEPart *part;

   part = gtk_type_new (vde_part_get_type ());
   corba_part = (VDE_Part) create_vde_part (BONOBO_OBJECT (part));
   if (corba_part == CORBA_OBJECT_NIL) {
      gtk_object_destroy (GTK_OBJECT (part));
      return NULL;
   }
   bonobo_object_construct (BONOBO_OBJECT (part), corba_part);

   part->name = g_strdup (name);
   if (data) {
      part->uih_data = data;
   }
   return part;
}

void
vde_part_add_window (VDEPart *part, BonoboControl *control,
		     VDE_MasterPart_WindowType type, gchar *title)
{
   BonoboPropertyBag *bag;
   gushort *ptype;
   gchar *ptitle;
   gboolean *visibility;


   if (!(bag = bonobo_control_get_property_bag(control))) {
      bag = bonobo_property_bag_new ();
      bonobo_control_set_property_bag (control, bag);
   }

   ptitle = g_strdup (title);
   bonobo_property_bag_add (bag, "title", "string", (gpointer) ptitle, NULL,
			   _("The title of the control"),
			   BONOBO_PROPERTY_READ_ONLY);

   ptype = g_new0 (gushort, 1);
   *ptype = type;
   bonobo_property_bag_add (bag, "window_type", "ushort", (gpointer) ptype, NULL,
			   _("Where to embed the control"),
			   BONOBO_PROPERTY_READ_ONLY);

   visibility = g_new0 (gboolean, 1);
   *visibility = FALSE;
   bonobo_property_bag_add (bag, "visible", "boolean", (gpointer) visibility,
			   (gpointer) & visibility,
			   _("Whether this control is shown"), 0);

   part->windows = g_list_append (part->windows, control);
}

void
vde_part_add_proppage (VDEPart *part, BonoboControl *control, gchar *path)
{
   BonoboPropertyBag *bag;

   bag = bonobo_property_bag_new ();
   bonobo_control_set_property_bag (control, bag);

   bonobo_property_bag_add (bag, "path", "string", (gpointer) path, NULL,
			   _("the path of the property page"),
			   BONOBO_PROPERTY_READ_ONLY);

   part->prop_pages = g_list_append (part->prop_pages, control);
}

POA_VDE_Part__epv *
vde_part_get_epv (void)
{
   POA_VDE_Part__epv *epv;

   epv = g_new0 (POA_VDE_Part__epv, 1);
   epv->_get_name = impl__get_name;
   epv->init = impl_init;

   return epv;
}

static void
init_part_corba_class (void)
{
   vde_part_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
   vde_part_vepv.VDE_Part_epv = vde_part_get_epv ();
}

static CORBA_char *
impl__get_name (PortableServer_Servant servant, CORBA_Environment *ev)
{
   VDEPart *part = VDE_PART (bonobo_object_from_servant (servant));

   return CORBA_string_dup (CORBAIFY_STRING (part->name));
}

static guint
propsig_handler (VDEEventConsumer *consumer, gshort class_id,
		 gshort event_id, gchar *description, CORBA_any msg,
		 VDEPart *part)
{
   gchar *class = "proppage";

//   if (class_id == GPOINTER_TO_INT (g_hash_table_lookup (part->event_classes,
//							  class))) {
   if (class_id == 2) {
      switch (event_id) {
      case 1:
	 // TODO: save settings
	 g_warning ("save settings");
	 return TRUE;
	 break;
      default:
	 g_warning ("set to existing settings");
	 break;
      }
   }
   return FALSE;
}

static gchar *
remove_ulines (const char *str)
{
   gchar *retval;
   const char *p;
   char *q;

   retval = g_new0 (char, strlen (str) + 1);

   q = retval;
   for (p = str; *p != '\0'; p ++) {
      if (*p != '_')
	 *q++ = *p;
   }
   return retval;
}

static void
parse_vde_uiinfo (BonoboUIHandler *uih, VDEUIInfo *uii)
{
   BonoboUIHandlerMenuItem *item, *peer;
   VDEUIInfo *curr_uii;
   gchar *path;
   int pos = -1;
   
   for (curr_uii = uii; curr_uii->parent_path != NULL; curr_uii++) {
      item = bonobo_ui_handler_menu_parse_uiinfo_one (&(curr_uii->data));
      path = g_strconcat (curr_uii->parent_path,
			  remove_ulines (curr_uii->data.label), NULL);

      if (curr_uii->override) {
	 pos = bonobo_ui_handler_menu_get_pos (uih, path);
	 /* don't do anything if the item already exists */
	 if (pos != -1) {
	    continue;
	 }
      }
      pos = -1;
      if (uii->pos == VDE_MENU_BEFORE) {
	 pos = 1;
      }
      g_free (path);
      if (curr_uii->relative_to_sibling) {
	 path = g_strconcat (curr_uii->parent_path,
			     curr_uii->relative_to_sibling, NULL);

	 pos = bonobo_ui_handler_menu_get_pos (uih, path);
	 g_free (path);
	 if ((pos != -1) && (uii->pos = VDE_MENU_AFTER)) {
	    pos++;
	 }
      }
      item->pos = pos;
      bonobo_ui_handler_menu_add_one (uih, curr_uii->parent_path, item);
   }
}

static void
impl_init (PortableServer_Servant servant,
	   CORBA_Object corba_master, CORBA_Environment *ev)
{
   VDEPart *part = VDE_PART (bonobo_object_from_servant (servant));
   Bonobo_UIHandler master_handler;

   part->master = gtk_type_new (bonobo_object_client_get_type ());
   bonobo_object_client_construct (BONOBO_OBJECT_CLIENT (part->master),
				  corba_master);

   g_warning ("adding windows");
   g_list_foreach (part->windows, (GFunc) register_window, part);
   g_warning ("adding proppages");
   g_list_foreach (part->prop_pages, (GFunc) register_proppage, part);

   if (part->uih_data) {
      part->handler = bonobo_ui_handler_new ();
      master_handler = VDE_MasterPart_get_ui_handler (corba_master, ev);
      bonobo_ui_handler_set_container (part->handler, master_handler);

      parse_vde_uiinfo (part->handler, part->uih_data);
   }

   if (part->prop_pages) {
      part->consumer = vde_event_consumer_new ();
      vde_event_consumer_connect (part->consumer, part->master);
      gtk_signal_connect (GTK_OBJECT (part->consumer), "on_push",
			  GTK_SIGNAL_FUNC (propsig_handler), part);
   }
   gtk_signal_emit (GTK_OBJECT (part),
		    vde_part_signals[ON_INIT], part->master);
}

static void
register_window (BonoboControl *control, VDEPart *part)
{
   CORBA_Environment ev;

   g_warning ("registering individual window");
   CORBA_exception_init (&ev);
   VDE_MasterPart_register_window (
	 bonobo_object_corba_objref (BONOBO_OBJECT (part->master)),
	 (Bonobo_Control) bonobo_object_corba_objref (BONOBO_OBJECT (control)),
	 &ev);
   CORBA_exception_free (&ev);
}

static void
register_proppage (BonoboControl *control, VDEPart *part)
{
   CORBA_Environment ev;

   g_warning ("registering individual proppage");
   CORBA_exception_init (&ev);
   VDE_MasterPart_register_proppage (
	 bonobo_object_corba_objref (BONOBO_OBJECT (part->master)),
	 (Bonobo_Control) bonobo_object_corba_objref (BONOBO_OBJECT (control)),
	 &ev);
   CORBA_exception_free (&ev);
}

static CORBA_Object
create_vde_part (BonoboObject *object)
{
   POA_VDE_Part *servant;
   CORBA_Environment ev;

   CORBA_exception_init (&ev);
   servant = (POA_VDE_Part *) g_new0 (VDEPart, 1);
   servant->vepv = &vde_part_vepv;

   POA_VDE_Part__init ((PortableServer_Servant) servant, &ev);

   if (ev._major != CORBA_NO_EXCEPTION) {
      g_free (servant);
      CORBA_exception_free (&ev);
      return CORBA_OBJECT_NIL;
   }
   CORBA_exception_free (&ev);
   return bonobo_object_activate_servant (object, servant);
}

static void
part_destroy (GtkObject *object)
{
   GList *l;
   VDEPart *part = VDE_PART (object);

   /* TODO: free all GLists, etc */
   gtk_object_destroy (GTK_OBJECT (part->master));
   GTK_OBJECT_CLASS (vde_part_parent_class)->destroy (object);
}

static void
part_class_init (VDEPartClass *klass)
{
   GtkObjectClass *object_class = (GtkObjectClass *) klass;

   vde_part_parent_class = gtk_type_class (bonobo_object_get_type ());

   vde_part_signals[ON_INIT] =
      gtk_signal_new ("on_init",
		      GTK_RUN_FIRST,
		      object_class->type,
		      GTK_SIGNAL_OFFSET (VDEPartClass, on_init),
		      gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);

   gtk_object_class_add_signals (object_class, vde_part_signals, LAST_SIGNAL);
   object_class->destroy = part_destroy;

   init_part_corba_class ();
}

static void
part_init (VDEPart *part)
{
   part->windows = NULL;
   part->prop_pages = NULL;
   part->master = NULL;
   part->handler = NULL;
}
