/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#include <config.h>
#include <gtk/gtk.h>
#include <libgnorba/gnorba.h>
#include <bonobo.h>

#include "masterpart.h"
#include "event-consumer.h"
#include "vde.h"

enum {
   ON_PUSH,
   ON_DISCONNECT,
   LAST_SIGNAL
};

typedef struct {
   VDEEventConsumer *event_consumer;
   CORBA_any *event;
} IdleHandlerData;

static guint consumer_signals[LAST_SIGNAL];
static GtkObjectClass *vde_event_consumer_parent_class;

static void impl_push (PortableServer_Servant servant,
		       CORBA_any *data, CORBA_Environment *ev);
static void impl_disconnect_push_consumer (PortableServer_Servant servant,
					   CORBA_Environment *ev);
static CORBA_Object create_vde_event_consumer (BonoboObject *object);
static void event_consumer_destroy (GtkObject *object);
static void event_consumer_class_init (VDEEventConsumerClass *class);
static void event_consumer_init (VDEEventConsumer *event_consumer);
static guint send_event_signal (IdleHandlerData *data);

static POA_VDE_EventConsumer__vepv vde_event_consumer_vepv;

GtkType
vde_event_consumer_get_type (void)
{
   static GtkType type = 0;

   if (!type) {
      GtkTypeInfo info = {
	 "IDL:VDE/EventConsumer:1.0",
	 sizeof (VDEEventConsumer),
	 sizeof (VDEEventConsumerClass),
	 (GtkClassInitFunc) event_consumer_class_init,
	 (GtkObjectInitFunc) event_consumer_init,
	 NULL, NULL,
      };

      type = gtk_type_unique (bonobo_object_get_type (), &info);
   }
   return type;
}

/**
 * vde_event_consumer_new:
 *
 * Returns a new VDEEventConsumer object.
 */
VDEEventConsumer *
vde_event_consumer_new (void)
{
   VDE_EventConsumer corba_event_consumer;
   VDEEventConsumer *event_consumer;

   event_consumer = gtk_type_new (vde_event_consumer_get_type ());
   corba_event_consumer = (VDE_EventConsumer)
      create_vde_event_consumer (BONOBO_OBJECT (event_consumer));
   if (corba_event_consumer == CORBA_OBJECT_NIL) {
      gtk_object_destroy (GTK_OBJECT (event_consumer));
      return NULL;
   }
   bonobo_object_construct (BONOBO_OBJECT (event_consumer),
			   corba_event_consumer);

   return event_consumer;
}

/**
 * vde_event_consumer_connect:
 * @consumer: A VDEEventConsumer object
 * @masterpart: A VDEMasterPart object
 *
 * Use this function to connect your event consumer to the event channel
 * of the masterpart. This lets the consumer receive the events that
 * get passed to the event channel.
 */
void
vde_event_consumer_connect (VDEEventConsumer *consumer,
			    BonoboObjectClient *masterpart)
{
   CORBA_Environment ev;
   
   CORBA_exception_init (&ev);
   consumer->proxypushsupplier = (CORBA_Object)
      VDE_MasterPart_connect_listener (
	    bonobo_object_corba_objref (BONOBO_OBJECT (masterpart)),
	    bonobo_object_corba_objref (BONOBO_OBJECT (consumer)), &ev);
   
   if ((ev._major != CORBA_NO_EXCEPTION) ||
       (consumer->proxypushsupplier == CORBA_OBJECT_NIL)) {
   }
   CORBA_exception_free (&ev);
}

POA_VDE_EventConsumer__epv *
vde_event_consumer_get_epv (void)
{
   POA_VDE_EventConsumer__epv *epv;

   epv = g_new0 (POA_VDE_EventConsumer__epv, 1);
   return epv;
}

POA_CosEventComm_PushConsumer__epv *
vde_event_consumer_get_coseventepv (void)
{
   POA_CosEventComm_PushConsumer__epv *epv;

   epv = g_new0 (POA_CosEventComm_PushConsumer__epv, 1);
   epv->push = impl_push;
   epv->disconnect_push_consumer = impl_disconnect_push_consumer;

   return epv;
}

static void
init_consumer_corba_class (void)
{
   vde_event_consumer_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
   vde_event_consumer_vepv.CosEventComm_PushConsumer_epv =
      vde_event_consumer_get_coseventepv ();
   vde_event_consumer_vepv.VDE_EventConsumer_epv =
      vde_event_consumer_get_epv ();
}

static void
impl_push (PortableServer_Servant servant, CORBA_any *data,
	   CORBA_Environment *ev)
{
   gchar *str;
   IdleHandlerData *idle_data;
   
   VDEEventConsumer *consumer =
      VDE_EVENT_CONSUMER (bonobo_object_from_servant (servant));

   idle_data = g_new0 (IdleHandlerData, 1);
   idle_data->event_consumer = consumer;
   idle_data->event = CORBA_any__alloc ();
   CORBA_any__copy (idle_data->event, data);
   
   gtk_idle_add ((GtkFunction)send_event_signal, idle_data);
}

static void
impl_disconnect_push_consumer (PortableServer_Servant servant,
			       CORBA_Environment *ev)
{
}

static CORBA_Object
create_vde_event_consumer (BonoboObject *object)
{
   POA_VDE_EventConsumer *servant;
   CORBA_Environment ev;

   CORBA_exception_init (&ev);

   servant = (POA_VDE_EventConsumer *) g_new0 (VDEEventConsumer, 1);
   servant->vepv = &vde_event_consumer_vepv;

   POA_VDE_EventConsumer__init ((PortableServer_Servant) servant, &ev);

   if (ev._major != CORBA_NO_EXCEPTION) {
      g_free (servant);
      return CORBA_OBJECT_NIL;
   }
   return bonobo_object_activate_servant (object, servant);
}

static void
event_consumer_destroy (GtkObject *object)
{
   CORBA_Environment ev;
   VDEEventConsumer *event_consumer = VDE_EVENT_CONSUMER (object);

   CORBA_exception_init (&ev);

   if (!CORBA_Object_is_nil (event_consumer->proxypushsupplier, &ev)) {
      CORBA_Object_release (event_consumer->proxypushsupplier, &ev);
   }
   CORBA_exception_free (&ev);
   GTK_OBJECT_CLASS (vde_event_consumer_parent_class)->destroy (object);
}

typedef void (*GtkSignal_NONE__INT_INT_POINTER_POINTER) (GtkObject * object,
							 gint arg1, gint arg2,
							 gpointer arg3,
							 gpointer arg4,
							 gpointer user_data);

static void 
gtk_marshal_NONE__INT_INT_POINTER_POINTER (GtkObject * object,
					   GtkSignalFunc func,
					   gpointer func_data,
					   GtkArg * args)
{
   GtkSignal_NONE__INT_INT_POINTER_POINTER rfunc;
   
   rfunc = (GtkSignal_NONE__INT_INT_POINTER_POINTER) func;
   
   (*rfunc) (object, GTK_VALUE_INT (args[0]), GTK_VALUE_INT (args[1]),
	     GTK_VALUE_POINTER (args[2]), GTK_VALUE_POINTER (args[3]),
	     func_data);
}

static void
event_consumer_class_init (VDEEventConsumerClass *klass)
{
   GtkObjectClass *object_class = (GtkObjectClass *) klass;

   vde_event_consumer_parent_class =
      gtk_type_class (bonobo_object_get_type ());

   consumer_signals[ON_PUSH] =
      gtk_signal_new ("on_push",
		      GTK_RUN_FIRST,
		      object_class->type,
		      GTK_SIGNAL_OFFSET (VDEEventConsumerClass, on_push),
		      gtk_marshal_NONE__INT_INT_POINTER_POINTER,
		      GTK_TYPE_NONE,
		      4, GTK_TYPE_UINT, GTK_TYPE_UINT, GTK_TYPE_POINTER,
		      GTK_TYPE_POINTER);
   consumer_signals[ON_DISCONNECT] =
      gtk_signal_new ("on_disconnect",
		      GTK_RUN_FIRST,
		      object_class->type,
		      GTK_SIGNAL_OFFSET (VDEEventConsumerClass,
					 on_disconnect),
		      gtk_marshal_NONE__NONE, GTK_TYPE_NONE, 0);
   gtk_object_class_add_signals (object_class, consumer_signals, LAST_SIGNAL);

   klass->on_push = NULL;
   klass->on_disconnect = NULL;
   object_class->destroy = event_consumer_destroy;

   init_consumer_corba_class ();
}

static void
event_consumer_init (VDEEventConsumer *event_consumer)
{
   event_consumer->proxypushsupplier = CORBA_OBJECT_NIL;
}

static guint
send_event_signal (IdleHandlerData *data)
{
   VDE_Event *event;

   event = (VDE_Event*)data->event->_value;
   
   gtk_signal_emit (GTK_OBJECT (data->event_consumer),
		    consumer_signals[ON_PUSH], event->class_id,
		    event->event_id, event->description, event->data);
   CORBA_free (data->event);
   g_free (data);

   return FALSE;
}
