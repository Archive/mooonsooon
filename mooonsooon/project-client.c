/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#include <gnome.h>
#include <bonobo.h>

#include "project-client.h"

void
vde_project_client_register_mime_handler (BonoboObjectClient *project,
					  gchar *mime_type,
					  BonoboObject *handler,
					  gboolean override)
{
   CORBA_Environment ev;
   
   CORBA_exception_init (&ev);
   VDE_Project_register_mime_handler (
      bonobo_object_corba_objref (BONOBO_OBJECT (project)), mime_type,
      handler, override, &ev);
   CORBA_exception_free (&ev);
}

void
vde_project_client_unregister_mime_handler (BonoboObjectClient *project,
					    BonoboObject *handler)
{
   CORBA_Environment ev;
   
   CORBA_exception_init (&ev);
   VDE_Project_unregister_mime_handler (
      bonobo_object_corba_objref (BONOBO_OBJECT (project)), handler, &ev);
   CORBA_exception_free (&ev);
}

GList *
vde_project_client_get_target_list (BonoboObjectClient *project)
{
   CORBA_Environment ev;
   VDE_StringList *list;
   GList *ret = NULL;
   int i;
   
   CORBA_exception_init (&ev);
   list = VDE_Project__get_target_list (
      bonobo_object_corba_objref (BONOBO_OBJECT (project)), &ev);
   if (ev._major != CORBA_NO_EXCEPTION) {
      CORBA_exception_free (&ev);
      return NULL;
   }

   for (i = 0; i < list->_length; i++) {
      ret = g_list_prepend (ret, list->_buffer[i]);
   }

   CORBA_exception_free (&ev);
   CORBA_free (list);
   
   return ret;
}

BonoboObjectClient *
vde_project_client_get_target (BonoboObjectClient *project, gchar *name)
{
   BonoboObjectClient *target;
   VDE_Target corba_target;
   CORBA_Environment ev;
   
   CORBA_exception_init (&ev);
   corba_target = VDE_Project_get_target (
      bonobo_object_corba_objref (BONOBO_OBJECT (project)), name, &ev);
   
   if (ev._major != CORBA_NO_EXCEPTION) {
      CORBA_exception_free (&ev);
      return NULL;
   }
   CORBA_exception_free (&ev);
   
   target = gtk_type_new (bonobo_object_client_get_type ());
   bonobo_object_client_construct (target, corba_target);
   
   return target;
}

void
vde_project_client_remove_target (BonoboObjectClient *project, gchar *name)
{
   CORBA_Environment ev;
   
   CORBA_exception_init (&ev);
   VDE_Project_remove_target (
      bonobo_object_corba_objref (BONOBO_OBJECT (project)), name, &ev);
   CORBA_exception_free (&ev);
}

BonoboObjectClient *
vde_project_client_create_target (BonoboObjectClient *project, gchar *name,
				  gchar *prefix, VDE_Target_Type type)
{
   BonoboObjectClient *target;
   VDE_Target corba_target;
   CORBA_Environment ev;
   
   CORBA_exception_init (&ev);
   corba_target = VDE_Project_create_target (
      bonobo_object_corba_objref (BONOBO_OBJECT (project)), name, prefix,
      type, &ev);
   
   if (ev._major != CORBA_NO_EXCEPTION) {
      CORBA_exception_free (&ev);
      return NULL;
   }
   CORBA_exception_free (&ev);
   
   target = gtk_type_new (bonobo_object_client_get_type ());
   bonobo_object_client_construct (target, corba_target);
   
   return target;
}
