/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#ifndef __PROJECT_CLIENT_H__
#define __PROJECT_CLIENT_H__

#include <mooonsooon/vde-target.h>

BEGIN_GNOME_DECLS


void vde_project_client_register_mime_handler (BonoboObjectClient *project,
					       gchar *mime_type,
					       BonoboObject *handler,
					       gboolean override);
void vde_project_client_unregister_mime_handler (BonoboObjectClient *project,
						 BonoboObject *handler);
GList *vde_project_client_get_target_list (BonoboObjectClient *project);
BonoboObjectClient *vde_project_client_get_target (BonoboObjectClient *project,
						  gchar *name);
void vde_project_client_remove_target (BonoboObjectClient *project,
				       gchar *name);

BonoboObjectClient *vde_project_client_create_target (BonoboObjectClient *project,
						     gchar *name,
						     gchar *prefix,
						     VDE_Target_Type type);


END_GNOME_DECLS

#endif /* __PROJECT_CLIENT_H__ */
