#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="MooonSooon"

(test -f $srcdir/configure.in \
  && test -d $srcdir/idl \
  && test -d $srcdir/laser) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level prom directory"

    exit 1
}

. $srcdir/macros/autogen.sh
