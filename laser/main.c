/*  laser
 *  Copyright (c) 1999  Martijn van Beers
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include "config.h"

#include <gnome.h>
#include <glade/glade.h>
#include <libgnorba/gnorba.h>
#include <bonobo/gnome-bonobo.h>

#include "mooonsooon/part.h"

#include "project.h"
#include "prefs.h"
#include "lasertree.h"
#include "tree.h"
#include "target.h"
#include "tree-control.h"


CORBA_Environment ev;

static GnomeUIHandler *create_menus (void);

int
main (int argc, char **argv)
{
	VDEPart *part;
	GnomeUIHandler *uih;
	CORBA_ORB orb;
	CORBA_Object ns;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
//	gnome_CORBA_init_with_popt_table ("Laser", VERSION, &argc, argv,
//	                                  NULL, NULL, GNORBA_INIT_SERVER_FUNC, &ev);
	gnome_CORBA_init ("Laser", VERSION, &argc, argv,
	                  GNORBA_INIT_SERVER_FUNC, &ev);
	orb = gnome_CORBA_ORB ();

	if (bonobo_init (orb, CORBA_OBJECT_NIL, CORBA_OBJECT_NIL) == FALSE) {
		g_error ("can not bonobo_init\n");
	}
	
	uih = create_menus ();
	part = vde_part_new ("LaserTree", uih);
	/* add the project tree view */
	vde_part_add_window (part, laser_create_tree_control (),
	                     VDE_MasterPart_LIST_DOCK, _("Project view"));
	/* TODO: add the property pages */

	ns = gnome_name_service_get ();
	CORBA_exception_init (&ev);
	goad_server_register (ns,
			      gnome_object_corba_objref (GNOME_OBJECT (part)),
			      "laser", "server", &ev);
	CORBA_exception_free (&ev);
	bonobo_main ();
	CORBA_exception_free (&ev);

	return 0;
}

static GnomeUIHandler *
create_menus (void)
{
	GnomeUIHandler *uih;
	GnomeUIHandlerMenuItem *menu_list;
	GnomeUIHandlerToolbarItem *toolbar_list;

	uih = gnome_ui_handler_new ();
/*
	menu_list = gnome_ui_handler_menu_parse_uiinfo_list (main_menu_file);
	gnome_ui_handler_menu_add_list (uih, "/", menu_list);
	gnome_ui_handler_menu_free_list (menu_list);
	menu_list = gnome_ui_handler_menu_parse_uiinfo_list (main_menu_project);
	gnome_ui_handler_menu_add_list (uih, "/", menu_list);
	gnome_ui_handler_menu_free_list (menu_list);
*/
	/* probably add a toolbar first */
/*
	toolbar_list = gnome_ui_handler_toolbar_parse_uiinfo_list (file_toolbar);
	gnome_ui_handler_toolbar_add_list (uih, "File/", toolbar_list);
	gnome_ui_handler_toolbar_free_list (toolbar_list);
*/
}
