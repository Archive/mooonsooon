#include <EXTERN.h>
#include <perl.h>
#include <embed.h>
#undef _
#undef PACKAGE
#define _perl_dirty dirty
#undef dirty
#include <gnome.h>
#include "config.h"
#include "perl-embed.h"
#define dirty _perl_dirty
#undef _perl_dirty

static GHashTable *perl_to_id;
static GHashTable *perldir_to_id;
static GHashTable *id_to_data;
static GtkWidget *target_menu;
static GtkWidget *file_menu;
static PerlInterpreter *laser_perl_interp;


extern void xs_init(void);


void perl_init()
{
	char *argv[] = { "", NULL, NULL, NULL, NULL, NULL };
	char *dir, *localdir, *file;

	dir = LASER_DATADIR "/perl";
	argv[1] = g_strconcat ("-I", dir, NULL);
	localdir = gnome_util_prepend_user_home (".laser/perl");
	argv[2] = g_strconcat ("-I", localdir, NULL);
	argv[3] = g_strdup ("-P");
	file = g_concat_dir_and_file (localdir, "startup.pl");
	if (!g_file_exists (file)) {
		g_free (file);
		g_concat_dir_and_file (dir, "startup.pl");
	}
	g_free (localdir);
	if (!g_file_exists (file)) {
		g_error (
				_("Couldn't load %s. Please check if Laser is installed properly"),
				file);
	}
	argv[4] = file;

	laser_perl_interp = perl_alloc();
	perl_construct(laser_perl_interp);
	perl_parse(laser_perl_interp, xs_init, 4, argv, NULL);
	perl_run(laser_perl_interp);
}

int
perl_eval_va (char *str, ...)
{
    /* Evals a string, returns -1 if unsuccessful, else returns
     *  the number of return params
     *  char buf[10]; int a;
     *  perl_eval_va ("$a = 10; ($a, $a+1)",
     *                "i", &a,
     *                "s", buf,
     *                 NULL);
     */
       
    SV*       sv     = newSVpv(str,0);
    va_list   vl;
    char      *p     = NULL;  
    int       i      = 0; 
    int       nret   = 0;     /* number of return params expected*/
    int       result = 0;
    Out_Param op[20];
    int ii; double d;

    dSP;
    ENTER;
    SAVETMPS;
    PUSHMARK(sp);
    va_start (vl, str);

    while (p = va_arg(vl, char *)) {
        if ((*p != 's') && (*p != 'i') && (*p != 'd')) {
            fprintf (stderr, "perl_eval_va: Unknown option \'%c\'.\n"
                              "Did you forget a trailing NULL ?\n", *p);
            return -1;
        }
        op[nret].pdata = (void*) va_arg(vl, char *);
        op[nret++].type = *p;
    }
    va_end(vl);
    PUTBACK;
    result = perl_eval_sv(sv, (nret == 0) ? G_DISCARD :
                              (nret == 1) ? G_SCALAR  :
                                            G_ARRAY  );

    SPAGAIN;
    if (SvTRUE(GvSV(errgv))) { /* errgv == $@ */
        fprintf (stderr, "Eval error: %s", SvPV(GvSV(errgv), na)) ;
        return -1;
    }
    SvREFCNT_dec(sv);
    if (nret > result)
        nret = result;

    for (i = --nret; i >= 0; i--) {
        switch (op[i].type) {
        case 's':
            str = POPp;
            strcpy((char *)op[i].pdata, str);
            break;
        case 'i':
            ii = POPi;
            *((int *)(op[i].pdata)) = ii;
            break;
        case 'd':
            d = POPn;
            *((double *) (op[i].pdata)) = d;
            break;
        }
   }
   FREETMPS ;
   LEAVE;
   return result;
}    

void perl_deinit()
{
	perl_destruct(laser_perl_interp);
	perl_free(laser_perl_interp);
	laser_perl_interp = NULL;
}

void
perl_report_progress ()
{
/*
	GtkWidget *progress;
	gfloat val;
	GnomeAppBar *ab;

	ab = GNOME_APPBAR (GNOME_APP (laserP_view_win)->statusbar);
	if (GNOME_APPBAR_HAS_STATUS (ab)
	&& GNOME_APPBAR_INTERACTIVE (ab)) {
		progress = GTK_WIDGET (gnome_appbar_get_progress (ab));
		val = gtk_progress_get_value (GTK_PROGRESS (progress));
		val++;
		if (val > 100) {
			val = 0;
		}
		gtk_progress_set_value (GTK_PROGRESS (progress), val);
		gtk_widget_draw (progress, NULL);
	}
*/
}
