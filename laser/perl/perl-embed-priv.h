#ifndef __PERL_EMBED_PRIV_H__
#define __PERL_EMBED_PRIV_H__
typedef struct {
	char type;
	void *pdata;
} Out_Param;

void perl_clear_tree (void);
void perl_report_progress (void);
void perl_add_path (gchar *name, gchar *parent);
void perl_add_target (gchar *name, SV *tref, gchar *parent, gint type);
void perl_add_file (gchar *name, SV *tref);
int perl_eval_va (char *str, ...);
#endif /* __PERL_EMBED_PRIV_H__ */
