/*
 * Interface to Laser internal functions.
 */

#ifdef __cplusplus
extern "C" {
#endif
#include "EXTERN.h"
#include "perl.h"
#undef _
#undef PACKAGE
#undef dirty
#include "XSUB.h"
#ifdef __cplusplus
}
#endif

#ifdef PerlIO
typedef int SysRet;
typedef PerlIO * InputStream;
typedef PerlIO * OutputStream;
#else
typedef int SysRet;
typedef FILE * InputStream;
typedef FILE * OutputStream;
#endif

#include <glib.h>
#include "config.h"
#include "project.h"

MODULE = Laser  PACKAGE = Laser PREFIX = laser_

PROTOTYPES: ENABLE

void
laser_report_progress ()

void
laser_notify_status (string)
	gchar *string;

void
laser_menu_insert_item (path, label, func_name, pos)
	gchar *path;
	gchar *label;
	gchar *func_name;
	gint  pos;

gint
laser_menu_lookup_path (path)
	gchar *path;

MODULE = Laser  PACKAGE = Laser::Project  PREFIX = vde_project_

VDEProject *
new (CLASS, path)
	char *CLASS;
	gchar *path;
  CODE:
	RETVAL = vde_project_new(path);
  OUTPUT:
	RETVAL

void
vde_project_add_child (project, child)
	VDEProject *project;
	VDEProject *child;

void
vde_project_add_file (project, file)
	VDEProject *project;
	gchar *file;

void
vde_project_add_subst (project, name, value)
	VDEProject *project;
	gchar *name;
	gchar *value;

gchar *
vde_project_get_path (project)
	VDEProject *project;

void
vde_project_add_var (project, name, value)
	VDEProject *project;
	gchar *name;
	gchar *value;

gchar *
vde_project_get_var (project, name)
	VDEProject *project;
	gchar *name;



MODULE = Laser  PACKAGE = Laser::Makefile  PREFIX = laser_makefile_

LaserMakefile *
new (CLASS, path, project)
	char *CLASS;
	gchar *path;
	VDEProject *project
  CODE:
	RETVAL = laser_makefile_new (path);
  OUTPUT:
	RETVAL

char *
laser_makefile_get_path (makefile)
	LaserMakefile *makefile;

void
laser_makefile_add_target (makefile, target)
	LaserMakefile *makefile;
	VDETarget *target;



MODULE = Laser  PACKAGE = Laser::Target  PREFIX = vde_target_

VDETarget *
new (CLASS, name, type, prefix)
	char *CLASS;
	gchar *name;
	gchar *type;
	gchar *prefix;
  CODE:
	RETVAL = vde_target_new (name, type, prefix);
  OUTPUT:
	RETVAL

void
vde_target_add_file (target, name, flags)
	VDETarget *target
	gchar *name;
	gint flags;

void
vde_target_add_deps (target, list)
	VDETarget *target;
	GSList *list;
  CLEANUP:
	g_slist_free (list);

void
vde_target_add_libs (target, list)
	VDETarget *target;
	GSList *list;
  CLEANUP:
	g_slist_free (list);

void
vde_target_add_flags (target, list)
	VDETarget *target;
	GSList *list;
  CLEANUP:
	g_slist_free (list);
