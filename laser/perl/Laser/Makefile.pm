package Laser::Makefile;

use strict;

sub new {
	my ($object, $path, $project) = @_;
	my @target = ();
	my @lines = ();
	my $varref = $project->{"vars"};
	my %vars = %$varref;
	my @prefixes = ("bin", "sbin", "libexec", "data", "sysconf",
		               "sharedstate", "localstate", "lib", "info",
		               "lisp", "include", "oldinclude", "man",
		               "src", "pkglib", "pkginclude", "pkgdata",
		               "EXTRA", "noinst");
	my $makefile = {
		"path"     => $path,
		"target"   => \@target,
		"prefixes" => \@prefixes,
		"vars"     => \%vars
	};
	bless $makefile, 'Laser::Makefile';
	return $makefile;
}

sub get_path ($) {
	my $makefile = shift (@_);

	return $makefile->{"path"};
}

sub push_subdir (\%$) {
	my ($makefile, $dir) = @_;
	my $ref;

	if (!$makefile->{"subdirs"}) {
		$makefile->{"subdirs"} = [];
	}
	$ref = $makefile->{"subdirs"};
	push (@$ref, $dir);
}

sub get_subdirs (\%) {
	my $makefile = shift (@_);

	return $makefile->{"subdirs"};
}
	
sub push_target (\%$) {
	my ($makefile, $target) = @_;
	my $ref;

	if (!$makefile->{"targets"}) {
		$makefile->{"targets"} = [];
	}
	$ref = $makefile->{"targets"};
	push (@$ref, $target);
}

sub get_targets (\%) {
	my $makefile = shift (@_);

	return $makefile->{"targets"};
}

sub push_rule (\%$) {
	my ($makefile, $rule) = @_;
	my $ref;

	if (!$makefile->{"rules"}) {
		$makefile->{"rules"} = [];
	}
	$ref = $makefile->{"rules"};
	push (@$ref, $rule);
}

sub get_rules (\%) {
	my $makefile = shift (@_);

	return $makefile->{"rules"};
}

sub push_prefix ($$) {
	my ($makefile, $prefix) = @_;

	print @{$makefile->{"prefixes"}};
	print "(adding $prefix)\n";
	push (@{$makefile->{"prefixes"}}, $prefix);
}

sub get_prefixes (\%) {
	my $makefile = shift (@_);

	return $makefile->{"prefixes"};
}

sub push_var (\%$) {
	my ($makefile, $var, $value, $whitespace) = @_;
	my $ref;

	print "adding var $var (\"$whitespace\")\n";
#	if (!$makefile->{"vars"}) {
#		$makefile->{"vars"} = {};
#	}
	$ref = $makefile->{"vars"};
	$$ref{$var} = $value;
	print "setting var $var to", $$ref{$var}, "\n";
}

sub get_var (\%$) {
	my ($makefile, $var) = @_;

	my $hashref = $makefile->{"vars"};
	return $$hashref{$var};
}

sub get_vars (\%) {
	my $makefile = shift (@_);

	return $makefile->{"vars"};
}

sub push_if {
	my $makefile = shift (@_);
}

1;
