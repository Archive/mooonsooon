package Laser::Autoconf::Vars;

#
# Look near the end for the publicly accessible functions
#

BEGIN {
        use Exporter();
        use vars qw($VERSION @ISA);

        $VERSION = "1.00";
        @ISA = qw (Exporter);
}

use strict;
use Laser::Util ('subst_autoconf_vars', 'switch');


my $all_linguas = sub {
	my ($object, $list) = @_;
	my ($popath, @po, @list);

	$list =~ s/\"//g;
	@list = split (/\s+/, $list);

	# languages supported. check this against po/*.po
	$popath = $object->get_path . "/po";
	opendir (PODIR, $popath);
	print "PODIR = $popath\n";
	@po = grep (s/\.po$//, readdir (PODIR));

	@po = sort @po;
	@list = sort @list;

	my $equal = 0;
	if ($#po == $#list) {
		$equal = 1;
		for (my $i = 0; $i <= $#po; $i++) {
			if ($po[$i] != $list[$i]) {
				$equal = 0;
				last;
			}
		}
	}
	if ($equal == 0) {
		# TODO: warning
	}
};

Laser::Autoconf::register_var_handler ("ALL_LINGUAS", $all_linguas);

1;
