package Laser::Autoconf::Macros;

#
# Look near the end for the publicly accessible functions
#

BEGIN {
        use Exporter();
        use vars qw($VERSION @ISA);

        $VERSION = "1.00";
        @ISA = qw (Exporter);
}

use strict;
use Laser::Autoconf;
use Laser::Util ('subst_autoconf_vars', 'switch');



#			qr/AC_(?:CHECK|PATH)_(?:TOOL|PROG(?:S?))\(\s*(\w*)/ =>
#					[ $automake_vars ],

my $am_init_automake = sub {
	my ($object, $content) = @_;
	my (@substvars);

#	$content =~ /(\$?\(?[\w-]+\)?,\s*\[?(\$?\w+)/;
	$content =~ /(\$?\(?[\w-]+\)?),\s*\[?(\$?\w+)/;
	my ($package, $version) = ($1, $2);

	# remove quoting
	$version =~ s/\[(.*)\]/$1/g;
	$version =~ s/'(.*)'/$1/g;
	$version =~ s/"(.*)"/$1/g;

	@substvars = subst_autoconf_vars ($object, $version);
	$version = shift (@substvars);
	$object->add_subst ('VERSION', $version);
	@substvars = subst_autoconf_vars ($object, $package);
	$package = shift (@substvars);
	$object->add_subst ('PACKAGE', $package);
};

#			qr/AC_CONFIG_SUBDIRS\(\[?\s*(.*)\s*\[?\)/ =>
#					[ $config_subdirs, (\@subprojects) ],
my $ac_config_subdirs = sub {
	my ($object, $content) = @_;

	$content = /\[?\s*(.*)\s*\[?/;

	my $list = $1;
	push (@$Laser::Autoconf::projectref, split (/\s+/, $list));
#	print "subprojects: $list";
};

#			qr/AC_SUBST\((\w+)\)/ =>
#					[ $automake_vars ],
my $ac_subst = sub {
	my ($object, $content) = @_;
	my ($value, @subst);

	@subst = subst_autoconf_vars ($object, $content);
	$value = $object->get_var (shift (@subst));
	$object->add_subst ($content, $value);
};

#			qr/^AC_OUTPUT\(\[?\s*([^\),\]]*)/ =>
#					[ $ac_output, (\@makefiles) ],
my $ac_output = sub {
	my ($object, $content) = @_;
	my (@files);

	$content =~ /\[?\s*([^\),\]]*)/;
	my $list = $1;
	@files = split (/[\s\n]+/, $list);
	foreach my $file (@files) {
		if ($file =~ /Makefile$/) {
			push (@$Laser::Autoconf::makefileref, $file);
			print "add makefile $file\n";
		} else {
			$object->add_file ($file);
			print "add file $file\n";
		}
	}
};

my $ac_try_run = sub {
	my ($object, $content) = @_;

	print "content:$content;end\n";
	print "no match" unless $content =~ /\[(.*?)\]\s*,(.*)/s;
	my ($code, $line) = ($1, $2);

	print "code$code\nAC_TRY_RUN SUBPARSE\n$line\n(end of 'line'";
	Laser::Autoconf::_parse ($object, $line);
	print "end AC_TRY_RUN SUBPARSE\n";
};

Laser::Autoconf::register_macro_handler ("AM_INIT_AUTOMAKE", $am_init_automake);
Laser::Autoconf::register_macro_handler ("AC_CONFIG_SUBDIRS", $ac_config_subdirs);
Laser::Autoconf::register_macro_handler ("AC_SUBST", $ac_subst);
Laser::Autoconf::register_macro_handler ("AC_OUTPUT", $ac_output);
#Laser::Autoconf::register_macro_handler ("AC_TRY_RUN", $ac_try_run);
1;
