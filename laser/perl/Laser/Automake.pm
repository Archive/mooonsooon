package Laser::Automake;

BEGIN {
	use Exporter();
	use vars qw($VERSION @ISA);

	$VERSION = "1.00";
	@ISA = qw (Exporter);
}

use strict;
use FileHandle;
use Laser::Util ('is_in_list', 'canonicalize', 'switch');
use Laser::Makefile;
use Laser::Target;
use Laser::Autoconf;

# private variables

# targets by name
my %targets = ();
# lines in the makefile
my @lines = ();

my %target_types = ("PROGRAMS"    => 0,
		    "LIBRARIES"   => 1,
		    "LTLIBRARIES" => 2,
		    "DATA"        => 3,
		    "SCRIPTS"     => 4,
		    "HEADERS"     => 5);

# private functions

# Substitute vars with their content
my $subst_vars = sub {
	my ($makefile, $str) = @_;
	my ($subst);

	while ($str =~ /\$\((\w+)\)/) {
		$subst = $1;
		my $replacement = $makefile->get_var($subst);
		print "replacing $subst with $replacement\n";
		$str =~ s/\$\($subst\)/$replacement/;
	}
	return $str;
};

# Add local prefix
my $handle_prefix = sub {
	my ($makefile, $prefix, $content) = @_;
	my ($path, $top_path);

	$path = $makefile->get_path();
	$makefile->push_prefix ($prefix, $content);
};

my $add_filegroup = sub {
	my ($makefile, $fileflagref, $target, $group, $ws) = @_;

	$group =~ /\$\((.*)\)/;
	my $name = $1;
	print "adding filegroup $name\n";
	#$target->add_group ($name);
	my $names = &$subst_vars ($makefile, $group);
	foreach my $file (split (/(?<=\s)(?=[\w\$])/s, $names)) {
		$file =~ /([^\s\n\\]+)([\s\n\\]*)/;
		my ($fname, $ws) = ($1, $2);
		if ($name =~ /(BUILT_SOURCES|CLEANFILES)/) {
			$target->add_file ($name, $$fileflagref{$name}, $ws);
		} else {
			print "adding file $fname to $name\n";
		}
	}
};

# add to the list of targets
my $handle_aux_target = sub {
	my ($makefile, $fileflagref, $prefix, $type, $white, $names) = @_;
	my ($t, $target, $nr);

	$nr = $target_types{$type};
	print "AUX TARGET ($prefix, $type\[$nr])\n";
	#TODO: if it doesn't exist, add a prefix, and emit a warning
	if (is_in_list ($prefix, @{$makefile->{"prefixes"}})) {
		$target = new Laser::Target("aux", $nr, $prefix);
		foreach my $item (split (/(?<=\s)\b/, $names)) {
			my ($name, $ws);

			$item =~ /([^\s\n\\]+)([\s\n\\]*)/;
			($name, $ws) = ($1, $2);
			if ($name =~ /^\$\(/) {
				&$add_filegroup ($makefile, $fileflagref, $target, $name, $ws);
			} else {
				$target->add_file ($name, $$fileflagref{$name}, $ws);
			}
		}
		$makefile->push_target ($target);
	}
};

my $handle_maintarget = sub {
	my ($makefile, $prefix, $type, $white, $names) = @_;
	my ($target, $nr, $path);

	$nr = $target_types{$type};
	print "TARGETS $prefix, $type\[$nr]\n";
	$path = $makefile->get_path();
	#TODO: add the targetname and the whitespace
	#TODO: if it doesn't exist, add a prefix, and emit a warning
	if (is_in_list ($prefix, $makefile->{"prefixes"})) {
		foreach my $item (split (/(?<=\s)\b/, $names)) {
			my ($name, $ws);
			$item =~ /([^\s\\]+)([\s\\]*)/s;
			($name, $ws) = ($1, $2);
			print "TARGET $name\n";

			my $target = $targets{canonicalize($name)};
			$makefile->push_target ($target);
		}
	}
};

my $create_maintarget = sub {
	my ($prefix, $type, $names) = @_;

	my $nr = $target_types{$type};
	print "CREATE TARGET ";
	foreach my $t (split (/[\s\\]+/s, $names)) {
		my $target = new Laser::Target ($t, $nr, $prefix);
		my $name = canonicalize ($t);
		$targets{$name} = $target;
		print "registering target $name\n";
	}
};

# add SOURCES, DEPENDENCIES, LDADD and LDFLAGS sections to a target
my $handle_target_data = sub {
	my ($makefile, $fileflagref, $tname, $type, $white, $names) = @_;
	my ($target, $t, @list);

	print "target $tname";
	if (($target = $targets{$tname})) {
		print " found";
		# meant to do /[\s\\]+/s but without losing the whitespace
		# so using lookback/lookbehind
		@list = split (/(?<=\s)(?=[\w\$])/s, $names);
		if ($type eq 'SOURCES') {
			print " doing sources\n";
			# TODO: add the varname and whitespace
			#$names = &$subst_vars ($makefile, $names);
			foreach my $item (@list) {
				my ($name, $ws);
				$item =~ /([^\s\n\\]+)([\s\n\\]*)/;
				($name, $ws) = ($1, $2);
			}
		} elsif ($type eq 'DEPENDENCIES') {
			print " doing deps\n";
			$target->set_dependencies (\@list);
			foreach my $item (@list) {
				my ($name, $ws);
				$item =~ /([^\s\n\\]+)([\s\n\\]*)/;
				($name, $ws) = ($1, $2);
				print "dep: $name ($ws)\n";
			}
		} elsif ($type eq 'LIBADD' || $type eq 'LDADD') {
			print " doing libadd\n";
			$target->set_ldadd (\@list);
			foreach my $item (@list) {
				my ($name, $ws);
				$item =~ /([^\s\n\\]+)([\s\n\\]*)/;
				($name, $ws) = ($1, $2);
				print "add: $name ($ws)\n";
			}
		} elsif ($type eq 'LDFLAGS') {
			print " doing ldflags\n";
			$target->set_ldflags (\@list);
			foreach my $item (@list) {
				my ($name, $ws);
				$item =~ /([^\s\n\\]+)([\s\n\\]*)/;
				($name, $ws) = ($1, $2);
				print "flag: $name ($ws)\n";
			}
		}
	}
};

# Creates a string of flags for each file in the makefile
# currently the following flags are created
# 0x0001 --> the file is part of BUILT_SOURCES
# 0x0002 --> the file is part of CLEANFILES
# 0x0004 --> the file is part of MOSTLYCLEANFILES
# 0x0008 --> the file is part of DISTCLEANFILES
# 0x0010 --> the file is part of MAINTAINERCLEANFILES
# 0x0020 --> the file is part of EXTRA_DIST
my $handle_file_flag = sub {
	my ($makefile, $fileflagref, $var, $content) = @_;
	my ($val, $file);

	print "setting file flags\n";
	$content = &$subst_vars ($makefile, $content);
	if ($var eq 'BUILT_SOURCES') {
		$val = 0x0001;
	} elsif ($var eq 'CLEANFILES') {
		$val = 0x0002;
	} elsif ($var eq 'MOSTLYCLEANFILES') {
		$val = 0x0004;
	} elsif ($var eq 'DISTCLEANFILES') {
		$val = 0x0008;
	} elsif ($var eq 'MAINTAINERCLEANFILES') {
		$val = 0x0010;
	} elsif ($var eq 'EXTRA_DIST') {
		$val = 0x0020;
	} else {
		return;
	}
	foreach $file (split (/\s+/, $content)) {
		unless ($$fileflagref{$file}) {
			$$fileflagref{$file} = 0;
		}
		$$fileflagref{$file} |= $val;
	}
};


my $handle_if = sub {
	my ($makefile, $condition) = @_;

	$makefile->push_if ($condition);
};

my $handle_rule = sub {
	my ($makefile, $line) = @_;
	my (@list);

	do {
		chomp ($line);
		push (@list, $line);
		$line = shift (@lines);
	} while ($line =~ /(?:\@\w+\@)?^\t/);
	unshift (@lines, $line);
	$makefile->push_rule (\@list);
};

# private parse function
my $localparse = sub {
	my ($path, $project) = @_;
	my ($fh, @lines, $currentline, $makefile);
	my %fileflags;

	$fh = new FileHandle ($path, "r");
	if (!$fh) {
		# TODO: warn about not being able to read the file
		return;
		#return undef;
	}
print "NEW MAKEFILE\n";
	$makefile = new Laser::Makefile ($path, $project);

	%targets = ();
	@lines = ();
print "FIRST PASS\n";
	while ($fh && ($_ = $fh->getline())) {

#		Laser::report_progress();
		#TODO:
		# parsing if/then/else
		# substituting vars
		# much more


		# FIXME: Keep these
		next if (/^\#/); # skip comments
		next if (/^$/); # skip empty lines

		$currentline .= $_;
		$currentline =~ s/\#.*//; # remove comments at end of lines

		# concatenate \'d lines
		if ($currentline =~ /\\$/) {
			next;
		}

		if ($currentline =~ /^(\w+)(\s*\+?=[\s\\]*)(.*)/s) {
			my ($name, $whitespace, $content) = ($1, $2, $3);

			if ($name =~ /(?:BUILT_SOURCES|CLEANFILES|EXTRA_DIST)/) {
				&$handle_file_flag ($makefile, \%fileflags, $name, $content);
			}
			if ($name eq "BUILT_SOURCES" || ($name !~ /_(?:HEADERS|DATA|SCRIPTS|PROGRAMS|(?:LT)?LIBRARIES|SOURCES|DEPENDENCIES|LDADD|LIBADD|LDFLAGS)$/)) {
				$makefile->push_var ($name, $content, $whitespace);
			}
			# order isn't important. these can come after
			# (SOURCES|DEPENDENCIES|LDADD|LIBADD). *sigh*
			if ($name =~ /^([a-z_]+)_(PROGRAMS|LIBRARIES|LTLIBRARIES)/) {
				&$create_maintarget ($1, $2, $content);
			}
		}
		push (@lines, $currentline);
		$currentline = "";
	}
	$fh->close() if $fh;

print "SECOND PASS\n";
	while ($currentline = shift (@lines)) {
#		Laser::report_progress();
		switch ($currentline, [
			[ qr/^(\w+)dir\s*=\s*(.*)/ ,
					[ $handle_prefix ]],
			[ qr/^(\w+)_(HEADERS|DATA|SCRIPTS)(\s*=[\s\\]*)(.*)/s ,
					[ $handle_aux_target, \%fileflags ]],
			[ qr/^([a-z_]+)_(PROGRAMS|LIBRARIES|LTLIBRARIES)(\s*=[\s\\]*)(.*)/s ,
					[ $handle_maintarget ]],
			[ qr/^(\w+)_(SOURCES|DEPENDENCIES|LDADD|LIBADD|LDFLAGS)(\s*=[\s\\]*)(.*)/s ,
					[ $handle_target_data, \%fileflags ]],
			[ qr/\s*if(.*)/ ,
					[ $handle_if ]],
			[ qr/^((?:\@\w+\@)?[-\w\.\(\)\$]+:.*)/ ,
					[ $handle_rule ] ]
		], $makefile);
	}
#	return $makefile;
};

my $project_parse;
$project_parse = sub {
	my ($localpath, $parent) = @_;
	my ($mfref, $spref);

	print "$localpath:\n";

	my $p = new Laser::Project ($localpath);
	if ($parent) {
		$parent->add_child ($p);
	}
	# TODO: put project in the project-tree
	($mfref, $spref) = Laser::Autoconf::load($p);

	foreach my $makefile (@$mfref) {
		my $result;

		$makefile =~ m|(.*)/(\w+)/Makefile$|;
		my ($parent, $relpath) = ($1, $2);
		$makefile = "$localpath/" . $makefile . ".am";
		print "parsing $makefile\n";
		if (-e $makefile) {
			&$localparse ($makefile, $p);
		} else {
			#TODO: warn for non-existing file
		}
	}
	foreach my $subproject (@$spref) {
		# gotta have recursion somewhere it seems
		&$project_parse ("$localpath/$subproject", $p);
	}
};

# public functions

sub parse ($) {
	my $path = shift @_;

	&$project_parse ($path, undef);
}

sub save () {
	my ($makefile);

	print "Saving...";
}

1;
