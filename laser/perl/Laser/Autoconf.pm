package Laser::Autoconf;

#
# Look near the end for the publicly accessible functions
#

BEGIN {
        use Exporter();
        use vars qw($VERSION @ISA);

        $VERSION = "1.00";
        @ISA = qw (Exporter);
}

use strict;
use FileHandle;
use Laser::Util ('subst_autoconf_vars', 'switch');


$Laser::Autoconf::projectref;
$Laser::Autoconf::makefileref;
my %macros;
my %vars;

my $whitespace = sub {
	my ($object, $bleh) = @_;

	print "whitespace: $bleh";
};

my $comment = sub {
	my ($object, $type, $text) = @_;

	print "add comment";
};

my $storevar = sub {
	my ($object, $name, $value) = @_;

	$object->add_var ($name, $value);
	print "add var $name";
	if ($vars{$name} != "") {
		my $handler = $vars{$name};
		print "calling handler $name ($handler)\n";
		&$handler ($object, $value);
	}
};

my $storemacro = sub {
	my ($object, $name, $value) = @_;
	
	print "add macro $name";
	if ($macros{$name} != "") {
		my $handler = $macros{$name};
		print "calling handler $name ($handler)\n";
		&$handler ($object, $value);
	}
};

my $all_linguas = sub {
	my ($object, $list) = @_;
	my ($popath, @po, @list, %cmp, %hash);

	@list = split (/\s+/, $list);
	$popath = $object->get_path . "/po";
	# languages supported. check this against po/*.po
	opendir (PODIR, $popath);
	@po = grep (/\.po$/, readdir (PODIR));

	@cmp{@list, @po} = ();
	unless (@list == @po and keys %hash == @list) {
		# warning
	}
	# gets matched by storevar regexp too, so no
	# need to store it here
	return 1;
};

my $handle_if = sub {
	my ($object, $condition) = @_;
	print "if";
	return 1;
};

my $handle_else = sub {
	my ($object) = @_;
	print "else";
	return 1;
};

my $handle_fi = sub {
	my ($object) = @_;
	print "fi";
	return 1;
};

sub register_macro_handler {
	my ($name, $handler) = @_;

	print "registering handler for $name\n";
	$macros{$name} = $handler;
}
use Laser::Autoconf::Macros;

sub register_var_handler {
	my ($name, $handler) = @_;

	print "registering handler for $name\n";
	$vars{$name} = $handler;
}
use Laser::Autoconf::Vars;

sub load {
	my ($object) = @_;
	my $path = $object->{"path"};
	my ($fh, @substvars);
	my ($openparen, $closeparen, $openbrack, $closebrack);
	my $line;
	my (@makefiles, @subprojects);

	$Laser::Autoconf::projectref = \@subprojects;
	$Laser::Autoconf::makefileref = \@makefiles;
	$fh = new FileHandle ("$path/configure.in", "r");
	return undef unless $fh;

	$openparen = $closeparen = $openbrack = $closebrack = 0;
	my $incase = 0;
	my $inif = 0;
	while ($fh && ($_ = $fh->getline())) {
		my $test;
		$test = $_;
		$line .= $test;
		if ($openbrack == 0) {
			$test =~ s/^\s*#.*$//;
		}
		if ($test =~ /\bcase\b.*\bin\b/) {
			$incase++;
		}
		if ($openparen == 0 && $test =~ /^\s*\bif\b/) {
			$inif++;
			print "moreif ($inif)";
		}
		$openparen += $test =~ s/\(/(/g;
		$closeparen += $test =~ s/\)/)/g;
		$openbrack += $test =~ s/\[/[/g;
		$closebrack += $test =~ s/\]/]/g;
		# hack for perl var (seen in gtk+ configure.in)
		if ($test =~ /\$\]/) { $closebrack--; }
		if ($incase != 0 && $test =~ /;;$/ ) {
			$closeparen--;
		}
		if ($line =~ /\s*(.*)esac$/s) {
			my $check = $1;
			print "CHECK:$check;END";
			if ($check ne "" && $check =~ /.*\)((?!.*;;))/s) {
				print "===$1===";
				$closeparen--;
			}
			$incase--;
		}
		if ($openparen == 0 && $test =~ /^\s*\bfi\b/) {
			$inif--;
			print "$test lessif ($inif)";
		}
		print "paren($openparen|$closeparen) brack($openbrack|$closebrack)";
		print "$test iftest ($inif|$incase)$test\n";
		if (!(($openparen == $closeparen)
		&& ($openbrack == $closebrack))) {
			next;
		}
		$openparen = $closeparen = $openbrack = $closebrack = 0;
		if ($inif || $incase) {
			next;
		}
		if ($line =~ /^\s*$/s) {
			next;
		}
		$line = _parse ($object, $line);	
	}
	return (\@makefiles, \@subprojects);
}

sub _parse {
	my ($object, $line) = @_;

	$line =~ s/^(\s*)//s;
	my $whitespace = $1;
	if ($whitespace ne "") {
		$whitespace =~ s/\n/<-\n/gs;
		print "saving whitespace\"", $whitespace, "\"end\n";
	}

	while ($line !~ /^\s*$/s) {
		print "line ";
		my $tline = $line;
		$line = switch ($line, [
			[ qr/^(\#|dnl)(.*)/ , [ $comment ] ],
			[ qr/^if\s+(.*)\s*;\s*then(.*)fi/s , [ $handle_if ] ],
			[ qr/^case.*in(.*)esac/s , [ $handle_if ] ],
			# \b(?!=) means word-boundary, not followed by =
			# see perldoc perlre
			[ qr/^(\w+)\b(?!=)(?:\((.*)\))?/s , [ $storemacro ] ],
			[ qr/^(\w+)=(.*)/ , [ $storevar ] ],
		], $object);
		print "-->$tline";
		print "==>$line\n";
	}
	return $line;
}

sub save ($) {
	my ($object) = @_;

	print "Saving...";
}

1;
