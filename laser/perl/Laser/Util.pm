package Laser::Util;

BEGIN {
	use Exporter();
	use vars qw($VERSION @ISA @EXPORT_OK);

	@ISA = qw (Exporter);
	@EXPORT_OK = ('is_in_list', 'subst_autoconf_vars', 'canonicalize', 'switch');
	$VERSION = "0.01";
}

#private functions

my $pofiles = sub {
	my ($path) = @_;
	my (@files);

	open (FILE, "$path/po/POTFILES.in");
	@files = <FILE>;
	close (FILE);
	return @files;
};

sub is_in_list ($$) {
   my ($search, $arrayref) = @_;
   my ($str);

	$str = "." . join (".", @$arrayref) . ".";
	return ($str =~ /\.$search\./);
};

sub subst_autoconf_vars ($$) {
	my ($object, $str) = @_;
	my ($count, @retvars, $i, $subst, $var);

	@retvars = ();
	while ($str =~ /\$(\w+)/) {
		$var = $1;
		$subst = $object->get_var ($var);
		$str =~ s/\$$var/$subst/;
	}
	# add the final expansion to the front
	unshift (@retvars, $str);
	return @retvars;
}

sub canonicalize ($) {
        my ($str) = @_;

        $str =~ s/\W/_/g;
        return $str;
}

sub switch {
	my ($line, $listref, @params) = @_;
	my $hashref;

	foreach my $lr (@$listref) {
		my $re = $$lr[0];
		my $arr_ref = $$lr[1];
		my $subref = shift (@$arr_ref);
		my @regexparams;

		@regexparams = $line =~ $re;
		if (@regexparams) {
			unshift (@regexparams, @$arr_ref);
			&$subref (@params, @regexparams);
			$line =~ s/$re//;
			return $line;
		}
	}
}
1;
