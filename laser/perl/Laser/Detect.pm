use strict;

use DirHandle;
use IO::File;
package Laser::Detect;

BEGIN {
	use Exporter();
	use vars qw($VERSION @ISA @EXPORT_OK);

	@ISA = qw (Exporter);
	@EXPORT_OK = ('get_libs');
	$VERSION = "0.01";
}

my %registered_libs;

#forward declaration, so we can do recursiveness
my $register_lib;

$register_lib = sub {
	my ($name, $libsref) = @_;

	my ($fname, $lang, $deps, $desc) = split (/:/, $libsref->{$name});
	my @deps = split (/ /, $deps);

	my @regdeps;

	if ($#deps == -1) {
	} else {
		while (my $lib = shift (@deps)) {
			if ($registered_libs{$lib} eq "") {
				print "$lib not satisfied for $name\n";
				my $libstuff = $libsref->{$lib};
				if ($libstuff eq "") {
					#TODO: tell our caller
					print "eek! no such dep: $lib\n";
					print "not registering $name\n";
					return;
				}
				my $reglib = &$register_lib ($lib, $libsref);
				if ($reglib) {
					push (@regdeps, $reglib);
				}
			} else {
				push (@regdeps, $registered_libs{$lib});
			}
		}
	}
	if ($registered_libs{$name} eq "") {
		print "registering $name (@regdeps)\n";
		#TODO: do the actual registering ;)
		$registered_libs{$name} = "$name";
		return $name;
	}
};

sub get_libs {
	my (@categories) = @_;
	my (%libs, $cat);

	foreach $cat (@categories) {
		require "Laser/Detect/$cat.pm";
		my $func = "Laser::Detect::" . $cat . '::get_libs (\%libs)';
		eval "$func";
	}

	%registered_libs = ();
	foreach my $name (keys %libs) {
		&$register_lib ($name, \%libs);
	}
}

1;
