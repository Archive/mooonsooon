# temporary stubs for the gnome-build stuff
package Laser::Project;

use strict;

sub new ($) {
	my ($class, $path) = @_;

	my $project = {
		"path"     => $path,
		"vars"     => {}
	};
#	bless $project, 'Laser::Project';
	bless $project, $class;
	return $project;
}

sub get_path ($) {
	my $makefile = shift (@_);

	return $makefile->{"path"};
}

sub add_var {
	my ($obj, $name, $value) = @_;

#	$obj->{"vars"}->{$name} = $value;
	my $ref = $obj->{"vars"};
	$$ref{$name} = $value;
	print "setting var $name to", $$ref{$name}, "\n";

}

sub get_var {
	my ($obj, $name) = @_;

	return $obj->{"vars"}->{$name};
}

sub add_subst {
	my ($obj, $name, $value) = @_;

#	print "Adding subst var $name with value $value\n";
}

sub add_file {
	my ($obj, $file) = @_;

#	print "Adding file $file\n";
}

sub add_child {
	my ($obj, $child) = @_;
}

1;
