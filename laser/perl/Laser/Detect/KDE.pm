use strict;

use DirHandle;
use IO::File;
package Laser::Detect::KDE;

BEGIN {
	use Exporter();
	use vars qw($VERSION @ISA @EXPORT_OK);

	@ISA = qw (Exporter);
	@EXPORT_OK = ();
	$VERSION = "0.01";
}

my @no_k = ();

my $check_deps = sub {
	my ($deps, $depsref) = @_;
	my (@tmpdep);

	@tmpdep = split (/ /, $1);
	foreach (@tmpdep) {
		if (/.*lib([-\w]+)\.la$/) {
			my $lib = $1;
			push (@$depsref, $lib);
			if ($lib !~ /^k/) {
				push (@no_k, $lib);
			}
		}
	}
};

my $parse_file = sub {
	my ($f) = @_;
	my ($fh, @depends);

	$fh = new IO::File ($f);
	if (defined ($fh)) {
		my $line;
		while (defined ($line = $fh->getline())) {
			if ($line =~ /dependency_libs=\'(.*)'$/) {
				&$check_deps ($1, \@depends);
			}
		}
		undef $fh;
	}
	return @depends;
};

my $build_lib_list = sub {
	my ($libsref) = @_;

	my $dir = $ENV{'KDEDIR'} . "/lib";
	my $d = new DirHandle ($dir);
	if (defined ($d)) {
		my $p;
		while (defined ($p = $d->read)) {
			if ($p =~ /lib(k.*)\.la$/) {
				my @deps = &$parse_file ("$dir/$p");
				if ($libsref->{$1} eq "") {
					$libsref->{$1} = "$1:C++:@deps";
				}
			}
		}
		undef $d;
		foreach (@no_k) {
			my @deps = &$parse_file ("$dir/lib$_.la");
			if ($libsref->{$_} eq "") {
				$libsref->{$_} = "$_:C++:@deps";
			}
		}
	} else {
		# TODO: warn that KDEDIR wasn't set
	}
};

sub get_libs {
	my ($libs) = @_;
	@no_k = ();
	&$build_lib_list($libs);
}

1;
