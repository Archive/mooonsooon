use strict;

use DirHandle;
use IO::File;
use Laser::Detect::GnomeConfig;
package Laser::Detect::PkgConfig;

BEGIN {
	use Exporter();
	use vars qw($VERSION @ISA @EXPORT_OK);

	@ISA = qw (Exporter);
	@EXPORT_OK = ('get_libs');
	$VERSION = "0.01";
}

my $build_path = sub {
	my ($tmp, @tmppath);

	$tmp = $ENV{'PC_INCLUDEPATH'};
	push (@tmppath, split (/:/, $tmp));
	$tmp = $ENV{'GNOME_PATH'};
	push (@tmppath, grep { $_ .= "/libexec/pkgconfig" } split (/:/, $tmp));
	if ($#tmppath == -1) {
		$tmp = `pkg-config --print-pc-dir`;
		push (@tmppath, $tmp);
	}
	return @tmppath;
};

my $parse_file = sub {
	my ($f, $key) = @_;
	my ($fh, @depends, $lang, $name, $desc, $untranslated);

	$fh = new IO::File ($f);
	if (defined ($fh)) {
		my $line;
		while (defined ($line = $fh->getline())) {
			if ($line =~ /REQUIRES=\"(.*)\"/) {
				@depends = split(/\s/, $1);
			}
			if ($line =~ /LANGUAGE=\"(.*)\"/) {
				$lang = $1;
			}
			if ($line =~ /NAME=\"(.*)\"/) {
				$name = $1;
			}
			if ($line =~/$key=\"(.*)\"/) {
				$desc = $1;
			}
			if ($line =~/DESCRIPTION=\"(.*)\"/) {
				$untranslated = $1;
			}
		}
		undef $fh;
	}
	if ($desc eq "") {
		$desc = $untranslated;
	}
	return ($name, $lang, $desc, @depends);
};

my $build_lib_list = sub {
	my ($libsref) = @_;
	my (@path);

	my $key = 'DESCRIPTION';
	if ($ENV{'LANG'} ne "C") {
		$key .= "[$ENV{'LANG'}]";
	}
	@path = &$build_path();
	foreach my $dir (@path) {
		my $d = new DirHandle ($dir);
		if (defined ($d)) {
			my $p;
			while (defined ($p = $d->read)) {
				if ($p =~ /(.*)\.pc$/) {
					my $lib = $1;
					my ($name, $lang, $desc, @deps) = &$parse_file ("$dir/$p", $key);
					if ($libsref->{$lib} eq "") {
						$libsref->{$lib} = "$name:$lang:@deps:$desc";
					}
				}
			}
			undef $d;
		}
	}
};

sub get_libs {
	my ($libsref) = @_;
	&$build_lib_list($libsref);
	Laser::Detect::GnomeConfig::get_libs ($libsref);
}

1;
