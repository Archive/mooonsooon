use strict;

use DirHandle;
use IO::File;
package Laser::Detect::GnomeConfig;

BEGIN {
	use Exporter();
	use vars qw($VERSION @ISA @EXPORT_OK);

	@ISA = qw (Exporter);
	@EXPORT_OK = ('get_libs');
	$VERSION = "0.01";
}

my $build_path = sub {
	my ($tmp, @tmppath);

	$tmp = $ENV{'GNOME_LIBCONFIG_PATH'};
	push (@tmppath, split (/:/, $tmp));
	$tmp = $ENV{'GNOME_PATH'};
	push (@tmppath, grep { $_ .= "/lib" } split (/:/, $tmp));
	if ($#tmppath == -1) {
		$tmp = `gnome-config --libdir`;
		push (@tmppath, $tmp);
	}
	return (@tmppath);
};

my $build_lib_list = sub {
	my ($libsref) = @_;
	my (@path);

	@path = &$build_path();

	# evil code to match gnome-config evilness
	my %defaultlibs = ("glib" => "GLib:C::Library of data structs and utility functions",
			   "idl" => "IDL:C:poo:IDL include dirs for gnome",
			   "gnome" => "Gnome:C:glib:non-UI part of gnome-libs",
			   "gnomeui" => "Gnomeui:C:gnome gtk:GNOME widget library",
			   "gnorba" => "Gnorba:C:gnome:GNOME object activation",
			   "gtk" => "GTK+:C:glib:Gimp ToolKit",
			   "gtkxhtml" => "GtkXHTML:C:gnomeui:GNOME HTML widget",
			   "zvt" => "Zvt:C:gnomeui:GNOME terminal emulation library");
	foreach (keys %defaultlibs) {
		if ($libsref->{$_} eq "") {
			$libsref->{$_} = $defaultlibs{$_};
		}
	}
	foreach my $dir (@path) {
		my $d = new DirHandle ($dir);
		if (defined ($d)) {
			my $p;
			while (defined ($p = $d->read)) {
				if ($p =~/(.*)Conf\.sh$/) {
					if ($libsref->{$1} eq "") {
					$libsref->{$1} = "$1:C::$1";
					}
				}
			}
		}
	}
};

sub get_libs {
	my ($libsref) = @_;
	&$build_lib_list($libsref);
}

1;
