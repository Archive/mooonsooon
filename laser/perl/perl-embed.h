#ifndef __PERL_EMBED_H__
#define __PERL_EMBED_H__

typedef struct {
	char type;
	void *pdata;
} Out_Param;

void perl_init (void);
void perl_deinit (void);
void perl_report_progress (void);
int perl_eval_va (char *str, ...);


#endif /* __PERL_EMBED_H__ */
