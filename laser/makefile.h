#ifndef __MAKEFILE_H__
#define __MAKEFILE_H__

#include "target.h"

BEGIN_GNOME_DECLS

typedef struct {
	gchar *path;
	GHashTable *targets;
	GHashTable *vars;

	GNode *lines;
	GNode *inserthere;
} LaserMakefile;


LaserMakefile *laser_makefile_new          (gchar *path);
void laser_makefile_add_target (LaserMakefile *makefile, VDETarget *target);
void laser_makefile_add_var (LaserMakefile *makefile, gchar *name, gchar *value);

gchar *laser_makefile_get_path (LaserMakefile *makefile);

END_GNOME_DECLS
#endif /* __MAKEFILE_H__ */

