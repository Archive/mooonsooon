#include <gnome.h>
#include <glade/glade.h>

#include "config.h"
#include "lasertree.h"
#include "tree.h"
#include "dir.h"

static void dir_menu_on_add_target (GtkWidget *widget, gpointer data);
static void dir_menu_on_add_gui (GtkWidget *widget, gpointer data);
static void dir_menu_on_remove (GtkWidget *widget, gpointer data);
static void dir_menu_on_properties (GtkWidget *widget, gpointer data);

static GnomeUIInfo dir_menu_data [] = {
	{  GNOME_APP_UI_ITEM,
		N_("_New target"), N_("Add a target to this directory"),
		dir_menu_on_add_target, NULL, NULL,
		GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL
   },
	{  GNOME_APP_UI_ITEM,
		N_("_New target"), N_("Add a gui project to this directory"),
		dir_menu_on_add_gui, NULL, NULL,
		GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL
   },
	{	GNOME_APP_UI_ITEM,
		N_("_Remove"), N_("Remove this dir from the project"),
		dir_menu_on_remove, NULL, NULL,
		GNOME_APP_PIXMAP_NONE, NULL,
		0, 0, NULL
	},
		GNOMEUIINFO_MENU_PROPERTIES_ITEM (dir_menu_on_properties, NULL),
		GNOMEUIINFO_END
};

static void
laser_dir_create_menu (gpointer object, GtkMenuShell *menu)
{
	gnome_app_fill_menu_with_data (menu, dir_menu_data,
	                               NULL, TRUE, 0, object);
}

static void
dir_menu_on_add_target (GtkWidget *widget, gpointer data)
{
}

static void
dir_menu_on_add_gui (GtkWidget *widget, gpointer data)
{
}

static void
dir_menu_on_properties (GtkWidget *widget, gpointer data)
{
/*
	IdData *iddata = data;
	GtkWidget *dialog;
	GladeXML *xmldialog;

	xmldialog = glade_xml_new (LASER_DATADIR "/dirprops.glade", NULL);
	dialog = glade_xml_get_widget (xmldialog, "dirpropdialog");
	glade_xml_signal_autoconnect (xmldialog);
	gnome_dialog_run_and_close (GNOME_DIALOG (dialog));
*/
}

static void
dir_menu_on_remove (GtkWidget *widget, gpointer data)
{
}
