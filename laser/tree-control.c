#include <config.h>
#include <bonobo/gnome-control.h>

#include "lasertree.h"
#include "dir.h"
#include "target.h"
#include "file.h"

static gint tree_on_button_press (GtkCTree       *ctree,
                                  GdkEventButton *event,
                                  gpointer        data);
static void menu_kill            (GtkWidget      *widget,
                                  gpointer        dummy);


LaserTree *Plaser_tree;


GnomeControl *
laser_create_tree_control()
{
	GnomeControl *control;
	GtkWidget *scrolled_win;
	
	scrolled_win = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_win),
		GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_widget_show (scrolled_win);

	Plaser_tree = (LaserTree *) laser_tree_new ();
	gtk_signal_connect (GTK_OBJECT (Plaser_tree), "button_press_event",
		GTK_SIGNAL_FUNC(tree_on_button_press), NULL);
	gtk_clist_set_row_height (GTK_CLIST (Plaser_tree), 23);
	gtk_clist_set_column_width (GTK_CLIST (Plaser_tree), 0, 400);
	gtk_clist_set_selection_mode (GTK_CLIST (Plaser_tree),
		GTK_SELECTION_EXTENDED);
	gtk_widget_set_usize (GTK_WIDGET (Plaser_tree), 200, 300);
	gtk_container_add (GTK_CONTAINER (scrolled_win), GTK_WIDGET (Plaser_tree));
	gtk_widget_show (GTK_WIDGET (Plaser_tree));

	control = gnome_control_new (scrolled_win);

	return control;
}

static gint
viewP_on_button_press (GtkCTree       *ctree,
                       GdkEventButton *event,
                       gpointer        data)
{
	GtkCTreeNode *node;
	LaserTreeRowData *rowdata;
	gint row, col;


	gtk_clist_get_selection_info (GTK_CLIST (ctree), event->x, event->y,
	                              &row, &col);
	node = GTK_CTREE_NODE (g_list_nth (GTK_CLIST (ctree)->row_list, row));
	if (node == NULL) {
		return FALSE;
	}
	rowdata = (LaserTreeRowData *) gtk_ctree_node_get_row_data (ctree, node) ;
	if (event->button == 3) {
		GtkWidget *menu;

		gtk_clist_unselect_all (GTK_CLIST (ctree));
		gtk_ctree_select (GTK_CTREE (ctree), node);
		menu = gtk_menu_new();

		(*rowdata->fill_menu) (rowdata->object, GTK_MENU_SHELL(menu));
		gtk_signal_connect (GTK_OBJECT (menu), "deactivate",
		                    GTK_SIGNAL_FUNC (menu_kill), NULL);
		gtk_menu_popup (GTK_MENU(menu), NULL, NULL, NULL, NULL,
		                event->button, event->time);
      gtk_main ();
      gtk_widget_destroy (menu);
      return TRUE;
	}
	return FALSE;
}

static gint
tree_on_button_press (GtkCTree       *ctree,
                      GdkEventButton *event,
                      gpointer        data)
{
	GtkCTreeNode *node;
	LaserTreeRowData *rowdata;
	gint row, col;


	gtk_clist_get_selection_info (GTK_CLIST (ctree), event->x, event->y,
                                 &row, &col);
	node = GTK_CTREE_NODE (g_list_nth (GTK_CLIST (ctree)->row_list, row));
	if (node == NULL) {
   	return FALSE;
	}
	rowdata = (LaserTreeRowData *) gtk_ctree_node_get_row_data (ctree, node) ;
	if (event->button == 3) {
   	GtkWidget *menu;

		gtk_clist_unselect_all (GTK_CLIST (ctree));
		gtk_ctree_select (GTK_CTREE (ctree), node);
		menu = gtk_menu_new();

		(*rowdata->fill_menu) (rowdata->object, GTK_MENU_SHELL(menu));

		gtk_signal_connect (GTK_OBJECT (menu), "deactivate",
		                    GTK_SIGNAL_FUNC (menu_kill), NULL);
		gtk_menu_popup (GTK_MENU(menu), NULL, NULL, NULL, NULL,
		                event->button, event->time);
		gtk_main ();
		gtk_widget_destroy (menu);
		return TRUE;
	}
	return FALSE;
}

static void
menu_kill (GtkWidget *widget, gpointer dummy)
{
/*   g_warning ("destroying menu");     */
   gtk_main_quit ();
}

