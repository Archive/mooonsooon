#include <gnome.h>
#include <glade/glade.h>
#include <libgnorba/gnorba.h>
#include <bonobo/bonobo.h>
#include <bonobo/gnome-object.h>

#include "vde-target.h"
#include "config.h"
#include "lasertree.h"
#include "tree-control.h"
#include "target.h"
#include "file.h"

static GtkObjectClass *vde_target_parent_class;


static CORBA_Object create_vde_target (GnomeObject *object);
static void target_destroy (GtkObject *object);
static void target_class_init (VDETargetClass *klass);
static void target_init (VDETarget *target);

static VDE_Target_Type impl__get_target_type  (PortableServer_Servant  servant,
                                               CORBA_Environment      *ev);
static CORBA_char *impl__get_name             (PortableServer_Servant  servant,
                                               CORBA_Environment      *ev);
static void impl__set_name                    (PortableServer_Servant  servant,
                                               CORBA_char             *name,
                                               CORBA_Environment      *ev);
static CORBA_char *impl__get_prefix           (PortableServer_Servant  servant,
                                               CORBA_Environment      *ev);
static void impl__set_prefix                  (PortableServer_Servant  servant,
                                               CORBA_char             *name,
                                               CORBA_Environment      *ev);
static VDE_Target_FileList *impl__get_sources (PortableServer_Servant  servant,
                                               CORBA_Environment      *ev);
static VDE_StringList *impl__get_dependencies (PortableServer_Servant  servant,
                                               CORBA_Environment      *ev);
static VDE_StringList *impl__get_ldflags      (PortableServer_Servant  servant,
                                               CORBA_Environment      *ev);
static VDE_StringList *impl__get_ldadd        (PortableServer_Servant  servant,
                                               CORBA_Environment      *ev);
static void impl_add_file                     (PortableServer_Servant  servant,
                                               VDE_Target_FileList    *file,
                                               CORBA_Environment      *ev);
static void impl_remove_file                  (PortableServer_Servant  servant,
                                               VDE_Target_FileList    *file,
                                               CORBA_Environment      *ev);
static void impl_add_dependency               (PortableServer_Servant  servant,
                                               VDE_StringList         *dep,
                                               CORBA_Environment      *ev);
static void impl_remove_dependency            (PortableServer_Servant  servant,
                                               VDE_StringList         *dep,
                                               CORBA_Environment      *ev);
static void impl_add_ldflag                   (PortableServer_Servant  servant,
                                               VDE_StringList         *ldflag,
                                               CORBA_Environment      *ev);
static void impl_remove_ldflag                (PortableServer_Servant  servant,
                                               VDE_StringList         *ldflag,
                                               CORBA_Environment      *ev);
static void impl_add_ldadd                    (PortableServer_Servant  servant,
                                               VDE_StringList         *ldadd,
                                               CORBA_Environment      *ev);
static void impl_remove_ldadd                 (PortableServer_Servant  servant,
                                               VDE_StringList         *ldadd,
                                               CORBA_Environment      *ev);

static void create_menu (VDETarget *target, GtkMenuShell *menu);
void target_menu_new_file (GtkWidget *widget, gpointer target);
static void dummy_menu (GtkWidget *widget, VDETarget *target);
static void target_menu_properties (GtkWidget *widget, VDETarget *target);
static void add_to_list (gpointer value, gpointer data);


static GnomeUIInfo target_menu_data [] = {
		GNOMEUIINFO_MENU_NEW_ITEM (N_("_Add file"),
			N_("Add a file to this target"), target_menu_new_file, NULL),
		GNOMEUIINFO_MENU_PROPERTIES_ITEM (target_menu_properties, NULL),
		GNOMEUIINFO_END
};


static GnomeUIInfo bin_target_menu_data [] = {
		GNOMEUIINFO_SEPARATOR,
	{	GNOME_APP_UI_ITEM, N_("_Build"), N_("Build this target"),
		target_menu_new_file, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXEC,
		0, 0, NULL
	},
		GNOMEUIINFO_END
};

static GnomeUIInfo exebin_target_menu_data [] = {
	{	GNOME_APP_UI_ITEM, N_("_Run"), N_("Run this target"),
		target_menu_new_file, NULL, NULL,
		GNOME_APP_PIXMAP_NONE, NULL,
		0, 0, NULL
	},
		GNOMEUIINFO_END
};

static POA_VDE_Target__vepv vde_target_vepv;

GtkType
vde_target_get_type ()
{
	static GtkType type = 0;

	if (!type) {
		GtkTypeInfo info = {
			"IDL:VDE/Target:1.0",
			sizeof (VDETarget),
			sizeof (VDETargetClass),
			(GtkClassInitFunc) target_class_init,
			(GtkObjectInitFunc) target_init,
			NULL, NULL,
		};
		type = gtk_type_unique (gnome_object_get_type (), &info);
	}
	return type;
}

VDETarget *
vde_target_new (gchar *name, gchar *type, gchar *prefix)
{
	VDE_Target corba_target;
	VDETarget *target;

	/*TODO: the usual GnomeObject stuff */
	target = gtk_type_new (vde_target_get_type ());
	corba_target = (VDE_Target) create_vde_target (GNOME_OBJECT (target));
	if (corba_target == CORBA_OBJECT_NIL) {
		gtk_object_destroy (GTK_OBJECT (target));
		return NULL;
	}
	gnome_object_construct (GNOME_OBJECT (target), corba_target);

	return target;
}

void
vde_target_add_file (VDETarget *target, gchar *name, gint flags)
{
}

void
vde_target_remove_file (VDETarget *target, GSList *files)
{
	g_slist_foreach (files, (GFunc) laser_file_remove, target);
}

void
vde_target_add_deps (VDETarget *target, GSList *files)
{
	g_slist_foreach (files, (GFunc) laser_file_add, target);
}

void
vde_target_remove_deps (VDETarget *target, GSList *files)
{
	g_slist_foreach (files, (GFunc) laser_file_remove, target);
}

void
vde_target_add_ldflags (VDETarget *target, GSList *files)
{
	g_slist_foreach (files, (GFunc) laser_file_add, target);
}

void
vde_target_remove_ldflags (VDETarget *target, GSList *files)
{
	g_slist_foreach (files, (GFunc) laser_file_remove, target);
}

void
vde_target_add_libs (VDETarget *target, GSList *files)
{
	g_slist_foreach (files, (GFunc) laser_file_add, target);
}

void
vde_target_remove_libs (VDETarget *target, GSList *files)
{
	g_slist_foreach (files, (GFunc) laser_file_remove, target);
}

POA_VDE_Target__epv *
vde_target_get_epv (void)
{
	POA_VDE_Target__epv *epv;

	epv = g_new0 (POA_VDE_Target__epv, 1);
	epv->_get_target_type = impl__get_target_type;
	epv->_get_name = impl__get_name;
	epv->_set_name = impl__set_name;
	epv->_get_prefix = impl__get_prefix;
	epv->_set_prefix = impl__set_prefix;
	epv->_get_sources = impl__get_sources;
	epv->_get_dependencies = impl__get_dependencies;
	epv->_get_ldflags = impl__get_ldflags;
	epv->_get_ldadd = impl__get_ldadd;
	epv->add_file = impl_add_file;
	epv->remove_file = impl_remove_file;
	epv->add_dependency = impl_add_dependency;
	epv->remove_dependency = impl_remove_dependency;
	epv->add_ldflag = impl_add_ldflag;
	epv->remove_ldflag = impl_remove_ldflag;
	epv->add_ldadd = impl_add_ldadd;
	epv->remove_ldadd = impl_remove_ldadd;
}

static void
init_target_corba_class (void)
{
	vde_target_vepv.GNOME_Unknown_epv = gnome_object_get_epv ();
	vde_target_vepv.VDE_Target_epv = vde_target_get_epv ();
}

static CORBA_Object
create_vde_target (GnomeObject *object)
{
	POA_VDE_Target *servant;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	servant = (POA_VDE_Target *)g_new0 (VDETarget, 1);
	servant->vepv = &vde_target_vepv;

	POA_VDE_Target__init ((PortableServer_Servant) servant, &ev);
	if (ev._major != CORBA_NO_EXCEPTION) {
		g_free (servant);
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}
	CORBA_exception_free (&ev);
	return gnome_object_activate_servant (object, servant);
}

static void
target_destroy (GtkObject *object)
{
}

static void
target_class_init (VDETargetClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;

	vde_target_parent_class =
		gtk_type_class (gnome_object_get_type ());
	
	object_class->destroy = target_destroy;

	init_target_corba_class ();
}

static void
target_init (VDETarget *target)
{
}

static VDE_Target_Type
impl__get_target_type (PortableServer_Servant  servant,
                       CORBA_Environment      *ev)
{
}

static CORBA_char *
impl__get_name (PortableServer_Servant  servant,
                CORBA_Environment      *ev)
{
}

static void
impl__set_name (PortableServer_Servant  servant,
                CORBA_char             *name,
                CORBA_Environment      *ev)
{
}

static CORBA_char *
impl__get_prefix (PortableServer_Servant  servant,
                  CORBA_Environment      *ev)
{
}

static void
impl__set_prefix (PortableServer_Servant  servant,
                  CORBA_char             *name,
                  CORBA_Environment      *ev)
{
}

static VDE_Target_FileList *
impl__get_sources (PortableServer_Servant  servant,
                   CORBA_Environment      *ev)
{
}

static VDE_StringList *
impl__get_dependencies (PortableServer_Servant  servant,
                        CORBA_Environment      *ev)
{
}

static VDE_StringList *
impl__get_ldflags (PortableServer_Servant  servant,
                   CORBA_Environment      *ev)
{
}

static VDE_StringList *
impl__get_ldadd (PortableServer_Servant  servant,
                 CORBA_Environment      *ev)
{
}

static void
impl_add_file (PortableServer_Servant  servant,
               VDE_Target_FileList    *file,
               CORBA_Environment      *ev)
{
}

static void
impl_remove_file (PortableServer_Servant  servant,
                  VDE_Target_FileList    *file,
                  CORBA_Environment      *ev)
{
}

static void
impl_add_dependency (PortableServer_Servant  servant,
                     VDE_StringList         *dep,
                     CORBA_Environment      *ev)
{
}

static void
impl_remove_dependency (PortableServer_Servant  servant,
                        VDE_StringList         *dep,
                        CORBA_Environment      *ev)
{
}

static void
impl_add_ldflag (PortableServer_Servant  servant,
                 VDE_StringList         *ldflag,
                 CORBA_Environment      *ev)
{
}

static void
impl_remove_ldflag (PortableServer_Servant  servant,
                    VDE_StringList         *ldflag,
                    CORBA_Environment      *ev)
{
}

static void
impl_add_ldadd (PortableServer_Servant  servant,
                VDE_StringList         *ldadd,
                CORBA_Environment      *ev)
{
}

static void
impl_remove_ldadd (PortableServer_Servant  servant,
                   VDE_StringList         *ldadd,
                   CORBA_Environment      *ev)
{
}

static void
create_menu (VDETarget *target, GtkMenuShell *menu)
{
	gnome_app_fill_menu_with_data (menu, target_menu_data,
	                               NULL, TRUE, 0, target);
	
	switch (target->type) {
		case VDE_Target_LIBRARY:
		case VDE_Target_LTLIBRARY:
			gnome_app_fill_menu_with_data (menu, bin_target_menu_data, NULL,
					TRUE, g_list_length (menu->children), target);
		case VDE_Target_PROGRAM:
			gnome_app_fill_menu_with_data (menu, exebin_target_menu_data, NULL,
					TRUE, g_list_length (menu->children), target);
			break;
	}
}

void
target_menu_new_file (GtkWidget *widget, gpointer target)
{
	g_warning ("new file");
}

static void
dummy_menu (GtkWidget *widget, VDETarget *target)
{
}

static void
target_menu_properties (GtkWidget *widget, VDETarget *target)
{
	GtkWidget *dialog;
	GtkWidget *clist;
	GladeXML *xmldialog;


	xmldialog = glade_xml_new (LASER_DATADIR "/targetprops.glade", NULL);
	dialog = glade_xml_get_widget (xmldialog, "targetpropdialog");

	clist = glade_xml_get_widget (xmldialog, "deplist");
	g_list_foreach (target->deps, add_to_list, clist);

	clist = glade_xml_get_widget (xmldialog, "liblist");
	g_list_foreach (target->libs, add_to_list, clist);

	clist = glade_xml_get_widget (xmldialog, "linklist");
	g_list_foreach (target->linkflags, add_to_list, clist);

	glade_xml_signal_autoconnect (xmldialog);
	gnome_dialog_run_and_close (GNOME_DIALOG (dialog));
}

static void
add_to_list (gpointer value, gpointer data)
{
	gchar *text[1];

	text[0] = (gchar *) value;
	gtk_clist_append (GTK_CLIST (data), text);
}
