/* laser
 * Copyright (c) 1999  Martijn van Beers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gnome.h>
#include <glib.h>

#include <gtk/gtkclist.h>
#include <gtk/gtkctree.h>
#include "lasertree.h"
#include "mooonsooon/util.h"

/* FIXME: someone knowledgable set this to something reasonable please
 * (I just copied this from GtkCList)
 */
#define TREE_DATA_OPTIMUM_SIZE		512

/* gtk defaults */
static void treeP_class_init (LaserTreeClass *klass);
static void treeP_init       (LaserTree      *tree);


/* object overrides */
static void treeP_finalize ( GtkObject     *object);


/* clist overrides */
static void treeP_select_row   (GtkCList         *clist,
                                gint              row,
                                gint              column,
                                GdkEvent         *event);
static void treeP_unselect_row (GtkCList         *clist,
                                gint              row,
                                gint              column,
                                GdkEvent         *event);
static void treeP_remove_row   (GtkCList         *clist,
                                gint              row);

/* private functions */
LaserTreeRowData *treeP_new_rowdata (LaserTree        *tree,
                                     LaserTreeRowType  type);

enum
{
	SELECT_NAME,
	UNSELECT_NAME,
	LAST_SIGNAL
};

static GtkCTreeClass *parent_class = NULL;
static guint tree_signals[LAST_SIGNAL] = {0};


GtkType
laser_tree_get_type (void)
{
	static GtkType tree_type = 0;

	if (!tree_type) {
		static const GtkTypeInfo tree_info = {
			"LaserTree",
			sizeof (LaserTree),
			sizeof (LaserTreeClass),
			(GtkClassInitFunc) treeP_class_init,
			(GtkObjectInitFunc) treeP_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};
		tree_type = gtk_type_unique (GTK_TYPE_CTREE, &tree_info);
	}
	return tree_type;
}

static void
treeP_class_init (LaserTreeClass *klass)
{
/* TODO: add gtk_arg support */
	GtkObjectClass *object_class;
	GtkCListClass *clist_class;

	object_class = (GtkObjectClass *) klass;
	clist_class = (GtkCListClass *) klass;

	parent_class = gtk_type_class (GTK_TYPE_CTREE);

	object_class->finalize = treeP_finalize;


	tree_signals[SELECT_NAME] =
		gtk_signal_new ("select_name",
			GTK_RUN_FIRST,
			object_class->type,
			GTK_SIGNAL_OFFSET (LaserTreeClass, select_name),
			gtk_marshal_NONE__POINTER_INT,
			GTK_TYPE_NONE, 1, GTK_TYPE_LONG);
	tree_signals[UNSELECT_NAME] =
		gtk_signal_new ("unselect_name",
			GTK_RUN_FIRST,
			object_class->type,
			GTK_SIGNAL_OFFSET (LaserTreeClass, unselect_name),
			gtk_marshal_NONE__POINTER_INT,
			GTK_TYPE_NONE, 1, GTK_TYPE_LONG);

	gtk_object_class_add_signals (object_class, tree_signals, LAST_SIGNAL);

	clist_class->select_row = treeP_select_row;
	clist_class->unselect_row = treeP_unselect_row;
	clist_class->remove_row = treeP_remove_row;

	klass->select_name = NULL;
	klass->unselect_name = NULL;
}

static void
treeP_init (LaserTree *tree)
{
	tree->rows = g_hash_table_new (mooonsooon_short_hash,
			mooonsooon_short_equal);
	tree->next_id = 0;
	tree->rowdata_mem_chunk =
			g_mem_chunk_new ("lasertree rowdata memchunk",
	                       sizeof (LaserTreeRowData),
                          sizeof (LaserTreeRowData)
                          * TREE_DATA_OPTIMUM_SIZE,
                          G_ALLOC_AND_FREE);
}

GtkWidget *
laser_tree_new (void)
{
	GtkWidget *widget;
	LaserTree *tree;

	widget = gtk_type_new (LASER_TYPE_TREE);
	gtk_ctree_construct (GTK_CTREE(widget), 1, 0, NULL);

	tree = LASER_TREE (widget);
	gtk_clist_column_titles_hide (GTK_CLIST (widget));

	return widget;
};

gshort *
laser_tree_insert_row (LaserTree *tree, LaserTreeRowType row_type,
                      gshort id, gchar *name, gpointer object,
                      LaserMenuFillFunc menu_func)
{
	GtkCTreeNode *parent_node = NULL, *new_node;
	LaserTreeRowData *row_data;
	gchar *text[1];

	g_return_val_if_fail (tree != NULL, NULL);
	g_return_val_if_fail (LASER_IS_TREE (tree), NULL);

	if (id != -1) {
		parent_node = (GtkCTreeNode *) g_hash_table_lookup (tree->rows, &id);
	}

	text[0] = g_strdup (name);
	new_node = gtk_ctree_insert_node (GTK_CTREE (tree), parent_node, NULL, text,
			1, NULL, NULL, NULL, NULL, FALSE, (row_type == LASER_TREE_DIR_ROW) ? TRUE: FALSE);
	
	row_data = treeP_new_rowdata (tree, row_type);
	row_data->parent = parent_node;
	row_data->text = text[0];
	row_data->object = object;
	row_data->fill_menu = menu_func;

	gtk_ctree_node_set_row_data (GTK_CTREE (tree), new_node, row_data);
	g_hash_table_insert (tree->rows, &row_data->id, new_node);

	return &row_data->id;
}

void laser_tree_remove_row (LaserTree *tree, gshort id)
{
	GtkCTreeNode *node;
	LaserTreeRowData *row_data;

	g_return_if_fail (tree != NULL);
	g_return_if_fail (LASER_IS_TREE (tree));

	node = g_hash_table_lookup (tree->rows, &id);
	g_return_if_fail (node != NULL);

	row_data = gtk_ctree_node_get_row_data (GTK_CTREE (tree), node);
	g_free (row_data->parent);
	g_free (row_data->text);
	g_mem_chunk_free (tree->rowdata_mem_chunk, row_data);

	gtk_ctree_remove_node (GTK_CTREE (tree), node);
	g_hash_table_remove (tree->rows, &id);
}

void laser_tree_select_widget (LaserTree *tree, gshort id)
{
}

void laser_tree_deselect_widget (LaserTree *tree, gshort id)
{
}

static void
treeP_select_row (GtkCList *clist, gint row, gint column, GdkEvent *event)
{
	GList *node;

	g_return_if_fail (clist != NULL);
	g_return_if_fail (LASER_IS_TREE (clist));

	if ((node = g_list_nth (clist->row_list, row)) &&
			GTK_CTREE_ROW (node)->row.selectable)
		gtk_signal_emit_by_name (GTK_OBJECT (clist),
				"tree_select_row", node, column);
		gtk_signal_emit (GTK_OBJECT (clist), tree_signals[SELECT_NAME],
			LASER_TREE_ROW_DATA (GTK_CTREE_ROW (node)->row.data)->id);
}

static void
treeP_unselect_row (GtkCList *clist, gint row,
                         gint column, GdkEvent *event)
{
	GList *node;

	g_return_if_fail (clist != NULL);
	g_return_if_fail (LASER_IS_TREE (clist));

	if ((node = g_list_nth (clist->row_list, row)) &&
			GTK_CTREE_ROW (node)->row.selectable)
		gtk_signal_emit_by_name (GTK_OBJECT (clist),
				"tree_unselect_row", node, column);
		gtk_signal_emit (GTK_OBJECT (clist), tree_signals[UNSELECT_NAME],
			LASER_TREE_ROW_DATA (GTK_CTREE_ROW (node)->row.data)->id);
}

static void
treeP_finalize (GtkObject *object)
{
	g_return_if_fail (object != NULL);
	g_return_if_fail (LASER_IS_TREE (object));

	g_mem_chunk_destroy (LASER_TREE(object)->rowdata_mem_chunk);

  if (GTK_OBJECT_CLASS (parent_class)->finalize)
    (*GTK_OBJECT_CLASS (parent_class)->finalize) (object);
}

static void
treeP_remove_row (GtkCList *clist, gint row)
{
	GtkCTreeNode *node;
	LaserTreeRowData *row_data;

	g_return_if_fail (clist != NULL);
	g_return_if_fail (LASER_IS_TREE (clist));

	node = GTK_CTREE_NODE (g_list_nth (clist->row_list, row));

	if (node) {
		row_data = gtk_ctree_node_get_row_data (GTK_CTREE (clist), node);
		if (row_data)
			laser_tree_remove_row (LASER_TREE (clist), row_data->id);
	}
}

LaserTreeRowData *
treeP_new_rowdata (LaserTree *tree, LaserTreeRowType type)
{
	LaserTreeRowData *rowdata;

	rowdata = g_chunk_new (LaserTreeRowData, tree->rowdata_mem_chunk);
	rowdata->rowtype = type;
	rowdata->id = ++tree->next_id;
	return rowdata;
}
