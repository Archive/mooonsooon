#include <config.h>
#include <libgnorba/gnorba.h>
#include <gdk/gdkx.h>
#include <glade/glade.h>

#include "prefs.h"
#include "project.h"


typedef struct _PropWidgets {
	GtkWidget *topdir;
	GtkWidget *author;
	GtkWidget *email;
} PropWidgets;

static PropWidgets propwidgets;

static LaserProps local_props;
const LaserProps *laser_props = &local_props;

static GtkWidget *pref_dialog;

static void laser_prefs_save            (void);
static void prefsP_create               (void);
static void prefsP_load_pages           (GtkWidget        *propbox);
static void prefsP_load_part_prefs      (gpointer          part_name,
                                         gpointer          part,
                                         gpointer          dialog);
static void prefsP_create_external_page (gpointer          part,
                                         gchar            *page_name,
                                         gchar            *parent_name);
static GtkWidget * prefsP_pageone       (GtkWidget        *dialog);
static GtkWidget * prefsP_pagetwo       (GtkWidget        *dialog);
static GtkWidget * prefsP_pagethree     (GtkWidget        *dialog);
static void prefsP_on_changed           (GtkWidget        *widget,
                                         gpointer          data);
static void prefsP_on_apply             (GnomePropertyBox *box,
                                         gint              page);
static void prefsP_on_cancel            (void);


void
laser_prefs_init ()
{
	gchar *dir, *dirdef, *auth_default, *mail_default;
	gboolean useddefault;

	dir = gnome_util_prepend_user_home ("Projects/");
	dirdef = g_strconcat ("/laser/global/topdir=", dir, NULL);
	local_props.topdir = gnome_config_get_string_with_default (
			dirdef, &useddefault);
	g_free (dirdef);
	g_free (dir);

	auth_default = g_strconcat ("/laser/identity/author=",
			g_get_real_name(), NULL);
	local_props.author = gnome_config_get_string_with_default (
			auth_default, &useddefault);
	g_free (auth_default);

	mail_default = g_strconcat ("/laser/identity/email=",
			g_get_user_name(), "@", g_getenv ("HOSTNAME"), NULL);
	local_props.email = gnome_config_get_string_with_default (
			mail_default, &useddefault);
	g_free (mail_default);
}

void
laser_prefs_show (GtkWidget *widget, gpointer data)
{
/*
	prefsP_create();
	prefsP_load_pages(pref_dialog);
	gnome_dialog_run (GNOME_DIALOG(pref_dialog));
*/
}

static void
laser_prefs_save ()
{
/*
	laser_notify_status (_("Writing preferences..."));
	gnome_config_set_string ("/laser/global/topdir", laser_props->topdir);
	gnome_config_set_string ("/laser/identity/author", laser_props->author);
	gnome_config_set_string ("/laser/identity/email", laser_props->email);
	gnome_config_sync();
*/
}

static void
prefsP_create()
{
/*
	pref_dialog = laser_prop_box_new ();
	gtk_window_set_title (GTK_WINDOW (pref_dialog), _("Laser preferences"));
	gnome_dialog_close_hides (GNOME_DIALOG (pref_dialog), TRUE);
	gnome_dialog_set_parent (GNOME_DIALOG (pref_dialog),
	                         GTK_WINDOW (laserP_view_win));
	gtk_signal_connect (GTK_OBJECT (pref_dialog), "destroy",
			GTK_SIGNAL_FUNC (prefsP_on_cancel), pref_dialog);
	gtk_signal_connect (GTK_OBJECT (pref_dialog), "apply",
			GTK_SIGNAL_FUNC (prefsP_on_apply), pref_dialog);
	gtk_signal_connect (GTK_OBJECT (pref_dialog), "delete_event",
			GTK_SIGNAL_FUNC (gtk_false), NULL);
*/
}

static void
prefsP_load_pages (GtkWidget *propbox)
{
	/* local pages */
/*
	laser_prop_box_append_page (LASER_PROP_BOX(pref_dialog),
			prefsP_pageone(pref_dialog), "Laser", NULL);
	laser_prop_box_append_page (LASER_PROP_BOX(pref_dialog),
			prefsP_pagetwo(pref_dialog), _("User data"), "Laser");
	laser_prop_box_append_page (LASER_PROP_BOX(pref_dialog),
			prefsP_pagethree(pref_dialog), _("Projects"), "Laser");
*/
	/* external pages */

}

static void
prefsP_load_part_prefs (gpointer part_name, gpointer key, gpointer dialog)
{
	/* corba stuff deleted */
}

static void
prefsP_create_external_page (gpointer part, gchar *page_name, gchar *parent_name)
{
/*	GtkWidget *socket;
	CORBA_unsigned_long id;

	socket = gtk_socket_new ();
	id = GDK_WINDOW_XWINDOW (socket->window);
	* do the corba stuff *
	laser_prop_box_append_page (LASER_PROP_BOX(pref_dialog), socket,
			page_name, parent_name);
*/
}

static GtkWidget *
prefsP_pageone (GtkWidget *dialog)
{
	GladeXML *page;
	GtkWidget *result, *entry;

	page = glade_xml_new (LASER_DATADIR "/laserprefs.glade", "pageone");
	glade_xml_signal_autoconnect (page);
	result = glade_xml_get_widget (page, "pageone");

	entry = gnome_file_entry_gtk_entry (
			GNOME_FILE_ENTRY(glade_xml_get_widget (page, "topdirentry")));
	propwidgets.topdir = entry;
	gtk_entry_set_text (GTK_ENTRY (entry), laser_props->topdir);
	gtk_signal_connect (GTK_OBJECT (entry), "changed",
		GTK_SIGNAL_FUNC (prefsP_on_changed), dialog);

	return result;
}

static GtkWidget *
prefsP_pagetwo (GtkWidget *dialog)
{
	GladeXML *page;
	GtkWidget *result, *entry;

	page = glade_xml_new (LASER_DATADIR "/laserprefs.glade", "pagetwo");
	glade_xml_signal_autoconnect (page);
	result = glade_xml_get_widget (page, "pagetwo");

	entry = gnome_entry_gtk_entry (
			GNOME_ENTRY(glade_xml_get_widget (page, "nameentry")));
	propwidgets.author = entry;
	gtk_entry_set_text (GTK_ENTRY (entry), laser_props->author);
	gtk_signal_connect (GTK_OBJECT (entry), "changed",
		GTK_SIGNAL_FUNC (prefsP_on_changed), dialog);

	entry = gnome_entry_gtk_entry (
			GNOME_ENTRY(glade_xml_get_widget (page, "emailentry")));
	propwidgets.email = entry;
	gtk_entry_set_text (GTK_ENTRY (entry), laser_props->email);
	gtk_signal_connect (GTK_OBJECT (entry), "changed",
		GTK_SIGNAL_FUNC (prefsP_on_changed), dialog);

	return result;
}

static GtkWidget *
prefsP_pagethree (GtkWidget *dialog)
{
	GladeXML *page;
	GtkWidget *result, *entry;

	page = glade_xml_new (LASER_DATADIR "/laserprefs.glade", "pagethree");
	glade_xml_signal_autoconnect (page);
	result = glade_xml_get_widget (page, "pagethree");
	return result;
}

static void prefsP_on_changed (GtkWidget *widget, gpointer data)
{
	gnome_property_box_changed (GNOME_PROPERTY_BOX (data));
}

static void prefsP_on_apply             (GnomePropertyBox *box,
                                         gint              page)
{
/*
	switch (page) {
		case 0:
			g_free (local_props.topdir);
			local_props.topdir = g_strdup (gtk_entry_get_text (
					GTK_ENTRY (propwidgets.topdir)));
		break;
		case 1:
			g_free (local_props.author);
			local_props.author = g_strdup (gtk_entry_get_text (
					GTK_ENTRY (propwidgets.author)));
			g_free (local_props.email);
			local_props.email = g_strdup (gtk_entry_get_text (
					GTK_ENTRY (propwidgets.email)));
		break;
		case -1:
			laser_prefs_save();
		break;
		default:
		break;
	}
*/
}

static void prefsP_on_cancel            (void)
{
/*
	gtk_widget_destroy (GTK_WIDGET (pref_dialog));
*/
	propwidgets.topdir = NULL;
	propwidgets.author = NULL;
	propwidgets.email = NULL;
}
