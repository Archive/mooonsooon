#ifndef __PROJECT_H__
#define __PROJECT_H__

#include <gnome.h>
#include <bonobo/gnome-bonobo.h>
#include "vde-project.h"
#include "makefile.h"

BEGIN_GNOME_DECLS


#define VDE_PROJECT_TYPE        (vde_project_get_type ())
#define VDE_PROJECT(o)          (GTK_CHECK_CAST ((o), VDE_PROJECT_TYPE, VDEProject))
#define VDE_PROJECT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), VDE_PROJECT_TYPE, VDEProjectClass))
#define VDE_IS_PROJECT(o)       (GTK_CHECK_TYPE ((o), VDE_PROJECT_TYPE))
#define VDE_IS_PROJECT_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), VDE_PROJECT_TYPE))

typedef struct _VDEProject VDEProject;

struct _VDEProject {
	GnomeObject ancestor;

	gchar *path;
	GHashTable *file_handlers;
	GHashTable *targets;
	GSList *makefiles;
	GSList *processed_files;
	GSList *subprojects;
	GHashTable *vars;

	VDEProject *parent;
	GNode *lines;
	GNode *inserthere;
};

typedef struct {
	GnomeObjectClass parent_class;

} VDEProjectClass;

GtkType vde_project_get_type     (void);
VDEProject *vde_project_new          (gchar *path);

void vde_project_add_target (VDEProject *project, GList *targets);
void vde_project_add_child (VDEProject *project, VDEProject *child);
void vde_project_add_makefile (VDEProject *project, LaserMakefile *makefile);
void vde_project_add_file (VDEProject *project, gchar *file);
void vde_project_add_subst (VDEProject *project, gchar *name, gchar *value);
void vde_project_add_var (VDEProject *project, gchar *name, gchar *value);
void vde_project_remove_target (VDEProject *project, GList *targets);

gchar *vde_project_get_path (VDEProject *project);
gchar *vde_project_get_var (VDEProject *project, gchar *name);

POA_VDE_Project__epv *vde_project_get_epv (void);


END_GNOME_DECLS
#endif /* __PROJECT_H__ */

