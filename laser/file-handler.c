#include <gtk/gtksignal.h>
#include <gtk/gtkmarshal.h>
#include <bonobo/bonobo.h>
#include <bonobo/gnome-object.h>
#include "file_handler.h"

enum {
	ON_GETVERB,
	ON_DOVERB,
	LAST_SIGNAL
};

static guint signals [LAST_SIGNAL];
static GtkObjectClass *parent_class;

static CORBA_Object create_vde_file_handler (GnomeObject *object);
static void file_handler_destroy (GtkObject *object);
static void file_handler_class_init (VDEFileHandlerClass *class);
static void file_handler_init (VDEFileHandler *file_handler);
static VDE_StringList *impl_get_verb_list (PortableServer_Servant servant, CORBA_char *file, CORBA_Environment *ev);
static void impl_do_verb (PortableServer_Servant servant, CORBA_char *file, CORBA_char *verb, CORBA_Environment *ev);
static void impl_unregistered (PortableServer_Servant servant, CORBA_Environment *ev);


POA_VDE_FileHandler__epv vde_file_handler_epv = {
	NULL,
	&impl_get_verb_list,
	&impl_do_verb,
	&impl_unregistered
};

static
POA_VDE_FileHandler__vepv vde_file_handler_vepv = {
	&gnome_object_base_epv,
	&gnome_object_epv,
	&vde_file_handler_epv
};

GtkType
vde_file_handler_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		GtkTypeInfo info = {
			"IDL:VDE/FileHandler:1.0",
			sizeof (VDEFileHandler),
			sizeof (VDEFileHandlerClass),
			(GtkClassInitFunc) file_handler_class_init,
			(GtkObjectInitFunc) file_handler_init,
			NULL, NULL,
		};
		type = gtk_type_unique (gnome_object_get_type (), &info);
	}
	return type;
}

VDEFileHandler *
vde_file_handler_new (GnomeUIHandler *handler)
{
	VDE_FileHandler corba_file_handler;
	VDEFileHandler *file_handler;

	file_handler = gtk_type_new (vde_file_handler_get_type ());
	corba_file_handler = (VDE_FileHandler) create_vde_file_handler (GNOME_OBJECT (file_handler));
	if (corba_file_handler == CORBA_OBJECT_NIL) {
		gtk_object_destroy (GTK_OBJECT (file_handler));
		return NULL;
	}
	gnome_object_construct (GNOME_OBJECT (file_handler), corba_file_handler);

	return file_handler;
}

static CORBA_char *
impl_get_verb_list (PortableServer_Servant  servant,
                    CORBA_char             *file,
                    CORBA_Environment      *ev)
{
}

static void
impl_do_verb (PortableServer_Servant  servant,
              CORBA_char             *file,
              CORBA_char             *verb,
              CORBA_Environment      *ev)
{
}

static void
impl_unregistered (PortableServer_Servant  servant,
                   CORBA_Environment      *ev)
{
}

static void
vde_file_handler_on_init (VDEFileHandler *file_handler, GnomeObjectClient *master)
{
}

static CORBA_Object
create_vde_file_handler (GnomeObject *object)
{
	POA_VDE_FileHandler *servant;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	servant = (POA_VDE_FileHandler *)g_new0 (VDEFileHandler, 1);
	servant->vepv = &vde_file_handler_vepv;

	POA_VDE_FileHandler__init ((PortableServer_Servant) servant, &ev);

	if (ev._major != CORBA_NO_EXCEPTION) {
		g_free (servant);
		CORBA_Exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}
	CORBA_Exception_free (&ev);
	return gnome_object_activate_servant (object, servant);
}

static void
file_handler_destroy (GtkObject *object)
{
	GList *l;
	VDEFileHandler *file_handler = VDE_FILE_HANDLER (object);

	GTK_OBJECT_CLASS (vde_file_handler_parent_class)->destroy (object);
}

static void
file_handler_class_init (VDEFileHandlerClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;

	parent_class =
		gtk_type_class (gnome_object_get_type ());
	
	signals[ON_INIT] =
		gtk_signal_new ("on_init",
			GTK_RUN_FIRST,
			object_class->type,
			GTK_SIGNAL_OFFSET (VDEFileHandlerClass, on_init),
			gtk_marshal_NONE__NONE,
			GTK_TYPE_NONE, 0);

	gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);
	object_class->destroy = file_handler_destroy;

}

static void
file_handler_init (VDEFileHandler *file_handler)
{
}
