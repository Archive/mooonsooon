typedef struct {
	GList *makefiles;
} Project;

enum LineType {
	VAR,
	TARGET,
	RULE,
	IF,
	ELSE
};

typedef struct {
	LineType type;
	gpointer data;

} Makefile;

enum TargetType {
	TARGET_PROGRAM,
	TARGET_LIB,
	TARGET_LT,
	TARGET_HEADERS,
	TARGET_SCRIPTS,
	TARGET_DATA
};
typedef struct {
	gchar *name;
	TargetType *type;
	gchar *prefix;
	GList *sources;
	GList *deps;
	GList *ldflags;
	GList *ldadd;
} Target;
