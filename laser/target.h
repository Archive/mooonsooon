#ifndef __TARGET_H__
#define __TARGET_H__

#include <bonobo/bonobo.h>
#include <bonobo/gnome-object.h>
#include <bonobo/gnome-object-client.h>
#include "vde-target.h"

BEGIN_GNOME_DECLS

#define VDE_TARGET_TYPE        (vde_target_get_type ())
#define VDE_TARGET(o)          (GTK_CHECK_CAST ((o), VDE_TARGET_TYPE, VDETarget))
#define VDE_TARGET_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), VDE_TARGET_TYPE, VDETargetClass))
#define VDE_IS_TARGET(o)       (GTK_CHECK_TYPE ((o), VDE_TARGET_TYPE))
#define VDE_IS_TARGET_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), VDE_TARGET_TYPE))

typedef struct {
	GnomeObject parent;

	gchar *name;
	gshort tree_id;
	VDE_Target_Type type;
	gchar *prefix;
	GHashTable *sources;
	GList *deps;
	GList *linkflags;
	GList *libs;

	GnomeObject *project;
} VDETarget;

typedef struct {
	GnomeObjectClass parent_class;
} VDETargetClass;

GtkType vde_target_get_type     (void);
VDETarget *vde_target_new          (gchar *name, gchar *type, gchar *prefix);

void vde_target_add_file       (VDETarget *target, gchar *name, gint flags);
void vde_target_remove_file    (VDETarget *target, GSList *files);
void vde_target_add_deps       (VDETarget *target, GSList *flags);
void vde_target_remove_deps    (VDETarget *target, GSList *flags);
void vde_target_add_ldflags    (VDETarget *target, GSList *flags);
void vde_target_remove_ldflags (VDETarget *target, GSList *flags);
void vde_target_add_libs       (VDETarget *target, GSList *flags);
void vde_target_remove_libs    (VDETarget *target, GSList *flags);

#endif /* __TARGET_H__ */
