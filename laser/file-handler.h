#ifndef __FILE_HANDLER_H__
#define __FILE_HANDLER_H__

#include <bonobo/bonobo.h>
#include <bonobo/gnome-object.h>
#include <bonobo/gnome-object-client.h>
#include "vde-file_handler.h"

BEGIN_GNOME_DECLS

#define VDE_FILE_HANDLER_TYPE        (vde_file_handler_get_type ())
#define VDE_FILE_HANDLER(o)          (GTK_CHECK_CAST ((o), VDE_FILE_HANDLER_TYPE, VDEFileHandler))
#define VDE_FILE_HANDLER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), VDE_FILE_HANDLER_TYPE, VDEFileHandlerClass))
#define VDE_IS_FILE_HANDLER(o)       (GTK_CHECK_TYPE ((o), VDE_FILE_HANDLER_TYPE))
#define VDE_IS_FILE_HANDLER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), VDE_FILE_HANDLER_TYPE))


#define VDE_FILE_HANDLER_FACTORY(fn) ((VDEFileHandlerFactory)(fn))
typedef GtkWidget * (*VDEFileHandlerFactory)(void);

typedef struct {
	GnomeObject parent;

	GHashTable *verbs;
	VDEProject *project;
} VDEFileHandler;

typedef struct {
	GnomeObjectClass parent_class;

	void (*on_getverb) (VDEFileHandler *file_handler, gchar *file, GList *list);
	void (*on_doverb) (VDEFileHandler *file_handler, gchar *file, gchar *verb, gint *result);

} VDEFileHandlerClass;


GtkType vde_file_handler_get_type  (void);

VDEFileHandler *vde_file_handler_new (gchar *extension);
void vde_file_handler_add_verb (VDEFileHandler *handler, gchar *verb, VDEFileHandlerfunc func);
void vde_file_handler_remove_verb (VDEFileHandler *handler, gchar *verb);


extern POA_VDE_FileHandler__epv  vde_file_handler_epv;
extern POA_VDE_FileHandler__vepv vde_file_handler_vepv;

END_GNOME_DECLS
#endif /* __FILE_HANDLER_H__ */

