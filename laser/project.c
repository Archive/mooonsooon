#include <gtk/gtksignal.h>
#include <gtk/gtkmarshal.h>
#include <bonobo/gnome-bonobo.h>
#include "project.h"
#include "target.h"
#include "line.h"

static GtkObjectClass *vde_project_parent_class;

static CORBA_Object create_vde_project (GnomeObject *object);
static void project_destroy (GtkObject *object);
static void project_class_init (VDEProjectClass *class);
static void project_init (VDEProject *project);
static GnomeView *view_factory (GnomeEmbeddable *bonobo_object, const GNOME_ViewFrame view_frame, void *data);

static VDE_StringList *impl__get_target_list (PortableServer_Servant servant,
					      CORBA_Environment *ev);
static void impl_register_extension_handler (PortableServer_Servant servant,
					     CORBA_char *extension,
					     CORBA_Object handler,
					     CORBA_boolean override,
					     CORBA_Environment *ev);
static void impl_unregister_extension_handler (PortableServer_Servant servant,
					       CORBA_Object           handler,
					       CORBA_Environment      *ev);
static VDE_Target impl_get_target (PortableServer_Servant  servant,
				   CORBA_char             *name,
				   CORBA_Environment      *ev);
static void impl_remove_target (PortableServer_Servant  servant,
				CORBA_char             *name,
				CORBA_Environment      *ev);
static VDE_Target impl_create_target (PortableServer_Servant  servant,
				      CORBA_char             *name,
				      CORBA_Environment      *ev);



static POA_VDE_Project__vepv vde_project_vepv;

GtkType
vde_project_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		GtkTypeInfo info = {
			"IDL:VDE/Project:1.0",
			sizeof (VDEProject),
			sizeof (VDEProjectClass),
			(GtkClassInitFunc) project_class_init,
			(GtkObjectInitFunc) project_init,
			NULL, NULL,
		};
		type = gtk_type_unique (gnome_object_get_type (), &info);
	}
	return type;
}

VDEProject *
vde_project_new (gchar *path)
{
	VDE_Project corba_project;
	VDEProject *project;

	project = gtk_type_new (vde_project_get_type ());
	corba_project = (VDE_Project) create_vde_project (GNOME_OBJECT (project));
	if (corba_project == CORBA_OBJECT_NIL) {
		gtk_object_destroy (GTK_OBJECT (project));
		return NULL;
	}
	gnome_object_construct (GNOME_OBJECT (project), corba_project);

	project->path = path;
	return project;
}

void
vde_project_add_child (VDEProject *project, VDEProject *child)
{
   project->subprojects = g_slist_append (project->subprojects, child);
   child->parent = project;
}

void
vde_project_add_file (VDEProject *project, gchar *file)
{
   project->processed_files = g_slist_append (project->processed_files,
					      g_strdup (file));
}

void
vde_project_add_subst (VDEProject *project, gchar *name, gchar *value)
{
   gchar *storename, *storevalue;

   if (!g_hash_table_lookup (project->vars, name)) {
      storename = g_strdup (name);
      storevalue = g_strdup (value);
      g_hash_table_insert (project->automakevars, storename, storevalue);
   }
}

void
vde_project_add_var (VDEProject *project, gchar *name, gchar *value)
{
	gchar *storename, *storevalue;
	gpointer a, b;

	storename = g_strdup (name);
	storevalue = g_strdup (value);
	project->inserthere = laser_line_create (project->inserthere,
	                                         LASER_LINE_VAR,
	                                         storename, storevalue);
	if (!g_hash_table_lookup_extended (project->vars, name, &a, &b)) {
		g_hash_table_insert (project->vars, storename, project->inserthere);
	}
}

POA_VDE_Project__epv *
vde_project_get_epv (void)
{
	POA_VDE_Project__epv *epv;

	epv = g_new0 (POA_VDE_Project__epv, 1);
	epv->_get_target_list = impl__get_target_list;
	epv->register_extension_handler = impl_register_extension_handler;
	epv->unregister_extension_handler = impl_unregister_extension_handler;
	epv->get_target = impl_get_target;
	epv->remove_target = impl_remove_target;
	epv->create_target = impl_create_target;
}

static void
init_project_corba_class (void)
{
	vde_project_vepv.GNOME_Unknown_epv = gnome_object_get_epv ();
	vde_project_vepv.VDE_Project_epv = vde_project_get_epv ();
}

static void
add_target (VDETarget *target, VDEProject *project)
{
	g_hash_table_insert (project->targets, target->name, target);
}

void
vde_project_add_target (VDEProject *project, GList *targets)
{
	g_list_foreach (targets, (GFunc) add_target, project);
}

static void
remove_target (VDETarget *target, VDEProject *project)
{
}

void
vde_project_remove_target (VDEProject *project, GList *targets)
{
	g_list_foreach (targets, (GFunc) remove_target, project);
}

static VDE_StringList *
impl__get_target_list (PortableServer_Servant  servant,
                       CORBA_Environment      *ev)
{
}

static void
impl_register_extension_handler (PortableServer_Servant  servant,
                                 CORBA_char             *extension,
                                 CORBA_Object            handler,
											CORBA_boolean           override,
                                 CORBA_Environment      *ev)
{
	VDEProject *project = VDE_PROJECT (gnome_object_from_servant (servant));
	g_hash_table_insert (project->file_handlers, extension, handler);
}

static void
impl_unregister_extension_handler (PortableServer_Servant  servant,
                                   CORBA_Object            handler,
                                   CORBA_Environment      *ev)
{
}

static VDE_Target
impl_get_target (PortableServer_Servant  servant,
                 CORBA_char             *name,
                 CORBA_Environment      *ev)
{
}

static void
impl_remove_target (PortableServer_Servant  servant,
                    CORBA_char             *name,
                    CORBA_Environment      *ev)
{
}

static VDE_Target
impl_create_target (PortableServer_Servant  servant,
                    CORBA_char             *name,
                    CORBA_Environment      *ev)
{
}

static void
vde_project_on_init (VDEProject *project, GnomeObjectClient *master)
{
}

static CORBA_Object
create_vde_project (GnomeObject *object)
{
	POA_VDE_Project *servant;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	servant = (POA_VDE_Project *)g_new0 (VDEProject, 1);
	servant->vepv = &vde_project_vepv;

	POA_VDE_Project__init ((PortableServer_Servant) servant, &ev);

	if (ev._major != CORBA_NO_EXCEPTION) {
		g_free (servant);
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}
	CORBA_exception_free (&ev);
	return gnome_object_activate_servant (object, servant);
}

static void
project_destroy (GtkObject *object)
{
	GList *l;
	VDEProject *project = VDE_PROJECT (object);

	GTK_OBJECT_CLASS (vde_project_parent_class)->destroy (object);
}

static void
project_class_init (VDEProjectClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *) klass;

	vde_project_parent_class =
		gtk_type_class (gnome_object_get_type ());
	
	object_class->destroy = project_destroy;

	init_project_corba_class ();
}

static void
project_init (VDEProject *project)
{
}
