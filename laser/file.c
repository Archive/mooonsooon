#include <gnome.h>
#include <glade/glade.h>

#include "config.h"
#include "lasertree.h"
#include "tree-control.h"
#include "file.h"

static void file_menu_on_remove (GtkWidget *widget, gpointer data);
static void file_menu_on_properties (GtkWidget *widget, gpointer data);
static void create_menu (gpointer object, GtkMenuShell *menu);

static GnomeUIInfo file_menu_data [] = {
		GNOMEUIINFO_SEPARATOR,
	{	GNOME_APP_UI_ITEM, N_("_Remove"), N_("Remove this file from the target"),
		file_menu_on_remove, NULL, NULL,
		GNOME_APP_PIXMAP_NONE, NULL,
		0, 0, NULL
	},
		GNOMEUIINFO_MENU_PROPERTIES_ITEM (file_menu_on_properties, NULL),
		GNOMEUIINFO_END
};

void
laser_file_add (gchar *file, VDETarget *target)
{
	gshort *id;

	id = laser_tree_insert_row (Plaser_tree, LASER_TREE_FILE_ROW,
	                       target->tree_id, file, target, create_menu);
	g_hash_table_insert (target->sources, file, id);

}

void
laser_file_remove (gchar *file, VDETarget *target)
{
	gshort *id;

	id = g_hash_table_lookup (target->sources, file);
	laser_tree_remove_row (Plaser_tree, *id);
	g_hash_table_remove (target->sources, file);
}

static void
create_menu (gpointer object, GtkMenuShell *menu)
{
	/* TODO: check for VDE::FileHandler */
	gnome_app_fill_menu_with_data (menu, file_menu_data,
	                               NULL, TRUE, 0, object);
}

static void
file_menu_on_properties (GtkWidget *widget, gpointer data)
{
/*
	IdData *iddata = data;
	GtkWidget *dialog;
	GladeXML *xmldialog;

	xmldialog = glade_xml_new (LASER_DATADIR "/fileprops.glade", NULL);
	dialog = glade_xml_get_widget (xmldialog, "filepropdialog");
	glade_xml_signal_autoconnect (xmldialog);
	gnome_dialog_run_and_close (GNOME_DIALOG (dialog));
*/
}

static void
file_menu_on_remove (GtkWidget *widget, gpointer data)
{
}
