typedef enum {
	LASER_LINE_IF,
	LASER_LINE_ELSE,
	LASER_LINE_FI,
	LASER_LINE_MACRO,
	LASER_LINE_VAR,
	LASER_LINE_COMMENT,
	LASER_LINE_TARGETLIST,
	LASER_LINE_TARGET_DATA_LIST,
	LASER_LINE_TARGET_DATA_HASH,
	LASER_LINE_RULE,
	LASER_LINE_OTHER
} LaserLineType;

typedef struct {
	LaserLineType llt;
	gchar *name;
	gpointer value;
} LaserLine;

GNode *laser_line_create (GNode         *node,
                          LaserLineType  type,
                          gchar         *name,
                          gpointer       data);
