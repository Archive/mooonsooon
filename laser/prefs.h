#ifndef __PREFS_H__
#define __PREFS_H__

typedef struct _LaserProps {
	gchar *topdir;
	gboolean show_on_startup;

	gchar *author;
	gchar *email;

	gboolean integrated_includes;
} LaserProps;

extern const LaserProps *laser_props;

void laser_prefs_init (void);
void laser_prefs_show (GtkWidget *widget, gpointer data);
#endif /* __PREFS_H__ */

