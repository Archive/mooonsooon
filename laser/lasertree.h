/* laser
 * Copyright (c) 1999  Martijn van Beers
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef __LASER_TREE_H__
#define __LASER_TREE_H__

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C" {
#endif

#define LASER_TYPE_TREE (laser_tree_get_type ())
#define LASER_TREE(obj) \
	GTK_CHECK_CAST (obj, laser_tree_get_type (), LaserTree)
#define LASER_TREE_CLASS(klass) \
	GTK_CHECK_CLASS_CAST (klass, laser_tree_get_type (), LaserTreeClass)
#define LASER_IS_TREE(obj) \
	GTK_CHECK_TYPE (obj, laser_tree_get_type ())

#define LASER_TREE_ROW_DATA(_data_) ((LaserTreeRowData *)((_data_)))

typedef enum {
	LASER_TREE_DIR_ROW,
	LASER_TREE_TARGET_ROW,
	LASER_TREE_FILE_ROW
} LaserTreeRowType;

#define LASER_MENU_FILL_FUNC(fn) ((LaserMenuFillFunc)(fn))
typedef void (*LaserMenuFillFunc) (gpointer object, GtkMenuShell *menu);

typedef struct
{
	LaserTreeRowType rowtype;

	gshort id;
	GtkCTreeNode *parent;
	gchar *text;
	gpointer object;
	LaserMenuFillFunc fill_menu;
} LaserTreeRowData;

typedef struct
{
	GtkCTree ctree;

	gshort next_id;
	GHashTable *rows; /* so we can look up a row */

	GMemChunk *rowdata_mem_chunk;
} LaserTree;

typedef struct
{
	GtkCTreeClass parent_class;

	void (*select_name)   (LaserTree *tree,
	                       gshort    id);
	void (*unselect_name) (LaserTree *tree,
	                       gshort    id);
} LaserTreeClass;
GtkType laser_tree_get_type (void);
GtkWidget *laser_tree_new (void);

/* Use the macros defined above, not this function */
gshort *laser_tree_insert_row (LaserTree *tree,
                               LaserTreeRowType row_type,
                               gshort id,
                               gchar *name,
                               gpointer object,
                               LaserMenuFillFunc menu_func);
void laser_tree_remove_row    (LaserTree *tree,
                               gshort id);
void laser_tree_select_row    (LaserTree *tree,
                               gshort id);
void laser_tree_deselect_row  (LaserTree *tree,
                               gshort id);

#ifdef __cplusplus
}
#endif

#endif /* __LASER_TREE_H__ */
