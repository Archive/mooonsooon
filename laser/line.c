#include <glib.h>
#include "line.h"

static GNode *
add_line (GNode *parent, LaserLine *newline)
{
	switch (newline->llt) {
		case LASER_LINE_ELSE:
		case LASER_LINE_FI:
			return g_node_append_data (parent->parent, newline);
			break;
		default:
			return g_node_append_data (parent, newline);
			break;
	}
}


GNode *
laser_line_create (GNode *node, LaserLineType type, gchar *name, gpointer data)
{
	LaserLine *current, *new;

	new = g_new0 (LaserLine, 1);
	new->llt = type;
	new->name = name;
	new->value = data;

	current = node->data;
	switch (current->llt) {
		case LASER_LINE_IF:
		case LASER_LINE_ELSE:
			return add_line (node, new);
			break;
		default:
			return add_line (node->parent, new);
			break;
	}
}
