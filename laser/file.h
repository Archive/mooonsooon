#ifndef __FILE_H__
#define __FILE_H__

#include "target.h"

void laser_file_add (gchar *file, VDETarget *target);
void laser_file_remove (gchar *file, VDETarget *target);

#endif /* __FILE_H__ */
