/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#ifndef __LOGGER_H__
#define __LOGGER_H__

#include <bonobo.h>

#include <mooonsooon/vde-log.h>
#include <mooonsooon/event-consumer.h>
#include <mooonsooon/part.h>

BEGIN_GNOME_DECLS


#define VDE_LOG_TYPE        (vde_log_get_type ())
#define VDE_LOG(o)          (GTK_CHECK_CAST ((o), VDE_LOG_TYPE, VDELog))
#define VDE_LOG_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), VDE_LOG_TYPE, VDELogClass))
#define VDE_IS_LOG(o)       (GTK_CHECK_TYPE ((o), VDE_LOG_TYPE))
#define VDE_IS_LOG_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), VDE_LOG_TYPE))

typedef struct {
   BonoboObject parent;
} VDELog;

typedef struct {
   BonoboObjectClass parent_class;
} VDELogClass;

GtkType vde_log_get_type (void);

VDELog *vde_log_new (void);
POA_VDE_Log__epv *vde_log_get_epv (void);


END_GNOME_DECLS

#endif /* __LOGGER_H__ */
