#include <gnome.h>
#include <bonobo.h>
#include <zvt/zvtterm.h>
#include <glade/glade.h>

#include <mooonsooon/event-consumer.h>
#include <mooonsooon/part.h>
#include "logger.h"

static gboolean enabled = TRUE;

static void
test_toggle (GtkWidget *widget, VDEPart *part)
{
   enabled = !enabled;
}

static VDEUIInfo menus [] = {
   {  "/", NULL, VDE_MENU_BEFORE, TRUE,
      GNOMEUIINFO_MENU_FILE_TREE (NULL)
   },
   {  "/File/", NULL, VDE_MENU_BEFORE, FALSE,
      GNOMEUIINFO_ITEM_STOCK ("Open project", NULL, test_toggle,
			      GNOME_STOCK_MENU_OPEN)
   },
   {  "/File/", "Exit", VDE_MENU_BEFORE, FALSE,
      GNOMEUIINFO_ITEM_STOCK ("Close project", NULL, test_toggle,
			      GNOME_STOCK_MENU_CLOSE)
   },
   {  "/File/", "Open project", VDE_MENU_AFTER, FALSE,
      GNOMEUIINFO_ITEM_STOCK ("Save project", NULL, test_toggle,
			      GNOME_STOCK_MENU_SAVE)
   },
   {
      "/", "Windows", VDE_MENU_AFTER, TRUE,
      GNOMEUIINFO_MENU_SETTINGS_TREE (NULL)
   },
   {
      "/Settings/", "Preferences...", VDE_MENU_BEFORE, FALSE,
      GNOMEUIINFO_ITEM_STOCK ("toggle eventlog", NULL, test_toggle,
			      GNOME_STOCK_MENU_REVERT)
   },
   VDEUIINFO_END
};
   
static guint
logger_push_handler (VDEEventConsumer *consumer, gshort class_id,
		     gshort event_id, gchar *description, CORBA_any *msg, ZvtTerm *term)
{
   gchar *str;

   if (enabled) {
      str = g_strdup_printf ("event: %d, %s\n\r", event_id, description);
      zvt_term_feed (term, str, strlen (str));
      g_free (str);
   }
   return FALSE;
}

static void
logger_init_handler (VDEPart *part, ZvtTerm *zvt)
{
   if (!(part->consumer)) {
      part->consumer = vde_event_consumer_new ();
      vde_event_consumer_connect (part->consumer, part->master);
   }

   zvt_term_feed (zvt, "init\r\n", 6);
   gtk_signal_connect (GTK_OBJECT (part->consumer), "on_push",
		       GTK_SIGNAL_FUNC (logger_push_handler), zvt);
}

static BonoboControl *
create_proppage ()
{
   GladeXML *page;
   GtkWidget *widget;
   BonoboControl *control;
   
   page = glade_xml_new ("props.glade", "page1");
   glade_xml_signal_autoconnect (page);
   widget = glade_xml_get_widget (page, "page1");
   control = bonobo_control_new (widget);

   return control;
}
   
int
main (int argc, char **argv)
{
   BonoboControl *control;
   GtkWidget *hbox, *scrollbar;
   ZvtTerm *zvt;
   VDEPart *part;
   CORBA_ORB orb;
   CORBA_Environment ev;
   CORBA_Object ns;

   gnome_init ("logger", "0.1", argc, argv);
   orb = oaf_init (argc, argv);

   if (bonobo_init (orb, CORBA_OBJECT_NIL, CORBA_OBJECT_NIL) == FALSE) {
      g_error ("can not bonobo_init\n");
   }

   glade_gnome_init ();

   part = vde_part_new ("logger", menus);
   
   /* add the project log dock */
   hbox = gtk_hbox_new (0, 0);
   zvt = (ZvtTerm *) zvt_term_new_with_size (80, 10);
   scrollbar = gtk_vscrollbar_new (GTK_ADJUSTMENT (zvt->adjustment));
   gtk_box_pack_start (GTK_BOX (hbox), GTK_WIDGET (zvt), FALSE, FALSE, 0);
   gtk_box_pack_start (GTK_BOX (hbox), scrollbar, FALSE, FALSE, 0);
   control = bonobo_control_new (hbox);
   gtk_signal_connect (GTK_OBJECT (part), "on_init",
		       GTK_SIGNAL_FUNC (logger_init_handler), zvt);
   vde_part_add_window (part, control,
			VDE_MasterPart_DOCK, "Project Log");
   
   control = create_proppage ();
   vde_part_add_proppage (part, control, "Project Logger");

   oaf_active_server_register ("OAFIID:Part:20000126",
      bonobo_object_corba_objref (BONOBO_OBJECT (part)));

   bonobo_main ();
   return 0;
}
