/* MooonSooon
 * Copyright (C) 1999  Martijn van Beers
 * 
 * licenced under the terms of the GNU GPL. See COPYING for details
 */
#include <gtk/gtk.h>
#include <bonobo.h>

#include "logger.h"

static GtkObjectClass *vde_log_parent_class;

static CORBA_Object create_vde_log (BonoboObject *object);
static void log_class_init (VDELogClass *class);
static void log_init (VDELog *log);
static void impl_log_string (PortableServer_Servant servant,
			     CORBA_char *log,
			     CORBA_Environment *ev);

static POA_VDE_Log__vepv vde_log_vepv;

GtkType
vde_log_get_type (void)
{
   static GtkType type = 0;

   if (!type) {
      GtkTypeInfo info = {
	 "IDL:VDE/Log:1.0",
	 sizeof (VDELog),
	 sizeof (VDELogClass),
	 (GtkClassInitFunc) log_class_init,
	 (GtkObjectInitFunc) log_init,
	 NULL, NULL,
      };

      type = gtk_type_unique (bonobo_object_get_type (), &info);
   }
   return type;
}

VDELog *
vde_log_new ()
{
   VDE_Log corba_log;
   VDELog *log;

   log = gtk_type_new (vde_log_get_type ());
   corba_log = (VDE_Log) create_vde_log (BONOBO_OBJECT (log));
   if (corba_log == CORBA_OBJECT_NIL) {
      gtk_object_destroy (GTK_OBJECT (log));
      return NULL;
   }
   bonobo_object_construct (BONOBO_OBJECT (log), corba_log);
   return log;
}

POA_VDE_Log__epv *
vde_log_get_epv (void)
{
   POA_VDE_Log__epv *epv;

   epv = g_new0 (POA_VDE_Log__epv, 1);
   epv->log_string = impl_log_string;

   return epv;
}

static void
init_log_corba_class (void)
{
   vde_log_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
   vde_log_vepv.VDE_Log_epv = vde_log_get_epv ();
}

static void
impl_log_string (PortableServer_Servant servant, CORBA_char *logstring,
		 CORBA_Environment *ev)
{
   VDELog *log = VDE_LOG (bonobo_object_from_servant (servant));
}

static CORBA_Object
create_vde_log (BonoboObject *object)
{
   POA_VDE_Log *servant;
   CORBA_Environment ev;

   CORBA_exception_init (&ev);
   servant = (POA_VDE_Log *) g_new0 (VDELog, 1);
   servant->vepv = &vde_log_vepv;

   POA_VDE_Log__init ((PortableServer_Servant) servant, &ev);

   if (ev._major != CORBA_NO_EXCEPTION) {
      g_free (servant);
      CORBA_exception_free (&ev);
      return CORBA_OBJECT_NIL;
   }
   CORBA_exception_free (&ev);
   return bonobo_object_activate_servant (object, servant);
}

static void
log_class_init (VDELogClass *klass)
{
   GtkObjectClass *object_class = (GtkObjectClass *) klass;

   vde_log_parent_class = gtk_type_class (bonobo_object_get_type ());

   init_log_corba_class ();
}

static void
log_init (VDELog *log)
{
}
